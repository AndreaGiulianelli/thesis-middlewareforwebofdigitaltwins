package application;

import io.vertx.core.Vertx;
import verticles.WebSocketClientVerticle;

/**
 * Simple client that create a websocket for receiving twin events.
 *
 */
public final class RunClient {

    private RunClient() { }
    /**
     * @param strings params
     */
    public static void main(final String...strings) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new WebSocketClientVerticle());
    }
}
