// Declaration of the Gradle extension to use
plugins {
    java
    application
    id("com.github.johnrengelman.shadow") version "5.2.0"
}
repositories {
    jcenter() // Contains the whole Maven Central + other stuff
}

dependencies {
    //implementation("org.scream3r:jssc:2.8.0")
    implementation("org.apache.commons:commons-math3:3.6.1")
	implementation("io.vertx:vertx-core:4.0.3")
	implementation("io.vertx:vertx-web:4.0.3")
	implementation("io.vertx:vertx-mqtt:4.0.2")
	
	//implementation("com.microsoft.azure.digitaltwins.v2020_12_01:azure-mgmt-digitaltwins:1.0.0")
	//implementation("com.azure:azure-digitaltwins-core:1.1.1")
	//implementation("com.azure:azure-identity:1.3.5")
	
	implementation("com.microsoft.azure.digitaltwins.v2020_12_01:azure-mgmt-digitaltwins:1.0.0")
	implementation("com.azure:azure-digitaltwins-core:1.0.3")
	implementation("com.azure:azure-identity:1.2.5")
	implementation("com.microsoft.signalr:signalr:1.0.0")
	implementation("org.apache.jena:apache-jena-libs:4.1.0")
}

tasks.withType<Test> {
}

application {
    mainClassName = "application.RunNode"
}
