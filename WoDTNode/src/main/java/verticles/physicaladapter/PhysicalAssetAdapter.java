package verticles.physicaladapter;

import adtcrud.ADTAsyncCrud;
import adtcrud.TwinBuilderAsyncShadower;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import model.DigitalTwin;
import model.Patient;
import model.utility.ModelConverter;
import utils.HttpCode;
import verticles.physicaladapter.datahubadapter.DataHub;
import verticles.worldadapter.CoreEventDispatcher;
import verticles.worldadapter.InternalEventType;

/**
 * Physical Asset Adapter component of the Web of Digital Twin Node.
 * It collect data from the DataHub, convert it with respect to the model 
 * and it update the Digital Twins via SDK for Azure Digital Twins.
 */
public class PhysicalAssetAdapter extends AbstractVerticle {
    /**
     * Event Bus Address for new data in the shadowing process from the DataHub. 
     */
    public static final String PA_SHADOWING_NEW_DATA = "pa.shadowing.newdata";
    /**
     * Event Bus Address for Digital Twins relationships management in the shadowing process from the DataHub. 
     */
    public static final String PA_SHADOWING_RELATIONSHIPS = "pa.shadowing.relationships";

    //Credentials to access the Azure Digital Twins instace
    private static final String TENANT_ID = "e99647dc-1b08-454a-bf8c-699181b389ab";
    private static final String APP_ID = "41e252f3-a0a6-4e48-b8eb-16b18b2a44dc";
    private static final String APP_SECRET = "UcPgnywJK-A06BO-X3vQ3tjMiMg9RFmOP6";
    private static final String HOSTNAME = "https://Org1-TwinBuilder.api.neu.digitaltwins.azure.net/";

    private TwinBuilderAsyncShadower twinBuilderShadower;

    @Override
    public final void start() {
        final EventBus eventBus = vertx.eventBus();
        //Create a promise in order to understand when the connection request is satisfied.
        final Promise<Void> isConnectedToADT = Promise.promise();

        this.twinBuilderShadower = new ADTAsyncCrud();

        vertx.executeBlocking(promise -> {
            try {
                //Connect to Azure Digital Twins instance
                this.twinBuilderShadower.connect(PhysicalAssetAdapter.TENANT_ID, PhysicalAssetAdapter.APP_ID, PhysicalAssetAdapter.APP_SECRET, PhysicalAssetAdapter.HOSTNAME);
                promise.complete();
            } catch (Exception e) {
                promise.fail(e);
            }
        }, res -> {
            if (res.succeeded()) {
               isConnectedToADT.complete(); 
            } else {
                System.out.println("(PA) Problems in connecting to Azure Digital Twins instance");
            }
        });

        //Wait the connection to ADT and then start the adapter
        isConnectedToADT.future().onComplete(ar -> {
            if (ar.succeeded()) {
                //PA connected successfully to the Azure Digital Twins instance
                System.out.println("Physical Asset Adapter connected to ADT");
                //Create listener to start shadowing process for data
                eventBus.consumer(PhysicalAssetAdapter.PA_SHADOWING_NEW_DATA, this::handleMessage);
                //Create listener to start shadowing process for relationships
                eventBus.consumer(PhysicalAssetAdapter.PA_SHADOWING_RELATIONSHIPS, this::handleRelMessage);
            }
        });
    }

    private void handleRelMessage(final Message<Object> message) {
        final JsonObject jsonObj = new JsonObject(message.body().toString());
        System.out.println("New rel data: " + jsonObj.toString());
        //Make the request to the twinBuilder api with the data extracted from the message of the DataHub.
        this.twinBuilderShadower.createRelation(jsonObj.getString(DataHub.DATA_HUB_NEW_DATA_DT_ID), jsonObj.getString(DataHub.DATA_HUB_NEW_DATA_DT_TARGET), jsonObj.getString(DataHub.DATA_HUB_NEW_DATA_DT_REL_NAME)).onComplete(res -> {
                //Prepare the object that will contain the result of the request
                final JsonObject result = new JsonObject();
                if (res.succeeded()) {
                    switch (HttpCode.fromCode(res.result())) {
                        case OK:
                            result.put("code", HttpCode.OK.getCode());
                            break;
                        case BAD_REQUEST:
                            //Relation name not correct.
                            result.put("error", "request invalid, check model and the request format");
                            result.put("code", HttpCode.BAD_REQUEST.getCode());
                            break;
                        case NOT_FOUND:
                            //Source or target id specified not found.
                            result.put("error", "source or target id not found");
                            result.put("code", HttpCode.NOT_FOUND.getCode());
                            break;
                        default:
                            //The request has some problems.
                            result.put("code", HttpCode.BAD_REQUEST.getCode());
                            break;
                    }
                } else {
                    //Internal error
                    result.put("code", HttpCode.SERVER_ERROR.getCode());
                }
                //Reply with the result to the DataHub in order to inform the delivery
                message.reply(result.encode());
            });
    }

    private void handleMessage(final Message<Object> message) {
        final EventBus eventBus = vertx.eventBus();
        final JsonObject receivedJsonObj = new JsonObject(message.body().toString());
        System.out.println("New data: " + receivedJsonObj.toString());
        //Extract the type of Digital Twin from the payload in order to understand which model use.
        final String digitalTwinType = receivedJsonObj.getString(DataHub.DATA_HUB_NEW_DATA_DT_TYPE_LABEL);
        DigitalTwin digitalTwinToUpdate = null;

        try {
            digitalTwinToUpdate = ModelConverter.fromType(digitalTwinType);
            //Set the object that represent the Digital Twin model in order to create internally the JsonPatchDocument to do the update
            digitalTwinToUpdate.setFromJsonObject(receivedJsonObj);
        } catch (IllegalArgumentException e) {
            System.out.println("Model not recognised");
        }
        //Request the update to Azure Digital Twins, through the client created.
        if (digitalTwinToUpdate != null) {
            final String id = digitalTwinToUpdate.getId();
            this.twinBuilderShadower.updateDigitalTwin(digitalTwinToUpdate).onSuccess(res -> {
               System.out.println("Updated"); 

               //Inform the Core Event Dispatcher
               final JsonObject event = new JsonObject();
               event.put(CoreEventDispatcher.TYPE_LABEL, InternalEventType.UPDATE)
                    .put(CoreEventDispatcher.ID_LABEL, id);
               eventBus.send(CoreEventDispatcher.NEW_EVENT, event.encode());

               //Reply to DataHub
               message.reply(HttpCode.OK.getCode());
            }).onFailure(err -> {
                System.out.println(err);
                //Reply to DataHub
                message.reply(HttpCode.BAD_REQUEST.getCode());
            });
        }
    }

}
