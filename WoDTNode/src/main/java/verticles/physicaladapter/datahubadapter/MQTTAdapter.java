package verticles.physicaladapter.datahubadapter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.mqtt.MqttClient;
import io.vertx.mqtt.messages.MqttConnAckMessage;

public class MQTTAdapter extends AbstractVerticle {

    private static final int MQTT_PORT = 1883;
    private static final String ROOT_TOPIC = "wodt/org1/+";
    private static final String HOST_NAME = "localhost";

    @Override
    public final void start() {
        final EventBus eventBus = vertx.eventBus();
        final MqttClient client = MqttClient.create(vertx);
        //Connection to broker, for educational purpose in local (it's a Mosquitto broker)
        Future<MqttConnAckMessage> future = client.connect(MQTTAdapter.MQTT_PORT, MQTTAdapter.HOST_NAME);
        future.onComplete((ar) -> {
            if (ar.succeeded()) {
                client.publishHandler((msg) -> {
                    final String dtType = msg.topicName().split("/")[2];
                    final JsonObject msgPayload = msg.payload().toJsonObject();
                    //System.out.println("New data from MQTT of type " + dtType + ": " + msgPayload.encodePrettily());
                    eventBus.send(DataHub.DATA_HUB_NEW_DATA, msgPayload.put(DataHub.DATA_HUB_NEW_DATA_DT_TYPE_LABEL, dtType));
                }).subscribe(MQTTAdapter.ROOT_TOPIC, 0);
            }
        });
    }
}
