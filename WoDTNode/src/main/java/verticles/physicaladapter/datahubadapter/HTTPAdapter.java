package verticles.physicaladapter.datahubadapter;

import com.ctc.wstx.shaded.msv_core.util.Uri;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import utils.HttpCode;

public class HTTPAdapter extends AbstractVerticle {

    private static final int PORT = 7979;

    private EventBus eventBus;

    @Override
    public final void start() {
        this.eventBus = vertx.eventBus();
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        //Create relationship between Digital Twin
        router.put("/api/twins/:sourceId/relationships/create").handler(this::createRelationship);
        router.put("/api/twins/:type/:dtId").handler(this::updateData);

        vertx.createHttpServer()
            .requestHandler(router)
            .listen(HTTPAdapter.PORT);
    }

    private void updateData(final RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        //Extract data from url
        final String type = routingContext.pathParam("type");
        final String dtId = routingContext.pathParam("dtId");
        try {
            final JsonObject jsonObj = routingContext.getBodyAsJson();
            if (jsonObj != null) {
                //Put mandatory fields
                jsonObj.put(DataHub.DATA_HUB_NEW_DATA_DT_ID, dtId)
                       .put(DataHub.DATA_HUB_NEW_DATA_DT_TYPE_LABEL, type);
                //Send data to DataHub
                eventBus.request(DataHub.DATA_HUB_NEW_DATA, jsonObj, ar -> {
                    if (ar.succeeded()) {
                       //It's sure that is integer convertible because is returned by the DataHub (so by the PhysicalAssetAdapter)
                       if (Integer.parseInt(ar.result().body().toString()) == HttpCode.OK.getCode()) {
                           //OK, succeeded
                           response.setStatusCode(HttpCode.OK.getCode())
                                   .end();
                       } else {
                           //Bad Request
                           final JsonObject error = new JsonObject();
                           error.put("error", "Check data, id and type and retry");
                           response.putHeader("content-type", "application/json")
                                   .setStatusCode(HttpCode.BAD_REQUEST.getCode())
                                   .end(error.encode());
                       }
                    } else {
                        //Internal error
                        response.setStatusCode(HttpCode.SERVER_ERROR.getCode())
                        .end();
                    }
                });
            }
        } catch (DecodeException e) {
            //Body not in JSON
            final JsonObject error = new JsonObject();
            error.put("error", "Body should be in JSON format");
            response.putHeader("content-type", "application/json")
                    .setStatusCode(HttpCode.BAD_REQUEST.getCode())
                    .end(error.encode());
        }
    }

    private void createRelationship(final RoutingContext routingContext) {
        System.out.println("relationship request");
        HttpServerResponse response = routingContext.response();
        final String sourceId = routingContext.pathParam("sourceId");
        //Check json body validity
        JsonObject jsonBody;
        try {
            jsonBody = routingContext.getBodyAsJson();
            if (jsonBody == null || !(jsonBody.containsKey("relation") && jsonBody.containsKey("target"))) {
                //Body malformed
                final JsonObject error = new JsonObject();
                error.put("error", "Body shoud specify relation and target");
                response.putHeader("content-type", "application/json")
                .setStatusCode(HttpCode.BAD_REQUEST.getCode())
                .end(error.encode());
            } else {
                jsonBody.put(DataHub.DATA_HUB_NEW_DATA_DT_ID, sourceId);
                this.eventBus.request(DataHub.DATA_HUB_RELATIONSHIPS, jsonBody, ar -> {
                    if (ar.succeeded()) {
                        //Reply with status code and error in case of fault.
                        final JsonObject result = new JsonObject(ar.result().body().toString());
                        final int statusCode = result.getInteger("code"); 
                        if (statusCode == HttpCode.OK.getCode()) {
                            //Success
                            response.setStatusCode(statusCode)
                                    .end();
                        } else {
                            //Error
                            response.putHeader("content-type", "application/json")
                                    .setStatusCode(statusCode)
                                    .end(new JsonObject().put("error", result.getString("error")).encode());
                        }
                    } else {
                        //Internal server error
                        response.setStatusCode(HttpCode.SERVER_ERROR.getCode())
                        .end();
                    }
                });
            }
        } catch (DecodeException e) {
            //Body of the request not in JSON
            final JsonObject error = new JsonObject();
            error.put("error", "Body shoud be in JSON format");
            response.putHeader("content-type", "application/json")
            .setStatusCode(HttpCode.BAD_REQUEST.getCode())
            .end(error.encode());
        } catch (Exception e) {
            System.out.println(e);
            response.setStatusCode(HttpCode.SERVER_ERROR.getCode())
            .end();
        }
    }

}
