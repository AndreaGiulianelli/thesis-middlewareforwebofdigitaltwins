package verticles.physicaladapter.datahubadapter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import verticles.physicaladapter.PhysicalAssetAdapter;

/**
 * DataHub is the hub for all the sources of IoT and device data.
 * It's essential for the shadowing process.
 * 
 * It's responsible for start all the adapter available, to collect the data and
 * to send this data to the physical asset adapter.
 * 
 * It collect also the data even if it's internal to the project in order to create
 * two different service that can be (one day) physically separate. 
 */
public class DataHub extends AbstractVerticle {
    /**
     * Event Bus Address for new incoming data.
     */
    protected static final String DATA_HUB_NEW_DATA = "datahub.newdata";
    /**
     * Event Bus Address for Digital Twins relationships management.
     */
    protected static final String DATA_HUB_RELATIONSHIPS = "datahub.relationships";
    /**
     * Label for specify Digital Twin type in the message data body.
     */
    public static final String DATA_HUB_NEW_DATA_DT_TYPE_LABEL = "DtType";
    /**
     * Label for specify Digital Twin ID in the message data body.
     */
    public static final String DATA_HUB_NEW_DATA_DT_ID = "DtId";
    /**
     * Label for specify Digital Twin target ID in relationships in the message data body.
     */
    public static final String DATA_HUB_NEW_DATA_DT_TARGET = "target";
    /**
     * Label for specify Digital Twin relationship name in the message data body.
     */
    public static final String DATA_HUB_NEW_DATA_DT_REL_NAME = "relation";

    @Override
    public final void start() {
        final EventBus eventBus = vertx.eventBus();
        //Deploy all the adapter verticles
        vertx.deployVerticle(new MQTTAdapter());
        vertx.deployVerticle(new HTTPAdapter());

        //Listen to incoming adapter's data. All the source will send data on this address.
        eventBus.consumer(DataHub.DATA_HUB_NEW_DATA, msg -> {
            //check that there is the id of the twin and the type
            final JsonObject jsonObj = new JsonObject(msg.body().toString());
            if (this.checkValidity(jsonObj)) {
                //Message validated
                //Send data to the Physical Asset Adapter
                eventBus.request(PhysicalAssetAdapter.PA_SHADOWING_NEW_DATA, jsonObj, ar -> {
                    if (ar.succeeded()) {
                        //Reply Physical Asset Adapter request's results to the adapter.
                        msg.reply(ar.result().body());
                    }
                });
            } //else nothing, message discarded.
        });

        //Listen to incoming adapter's data on relationships. All the source will send data on this address.
        eventBus.consumer(DataHub.DATA_HUB_RELATIONSHIPS, msg -> {
            final JsonObject jsonObj = new JsonObject(msg.body().toString());
            //check message validity
            if (this.checkRelValidity(jsonObj)) {
                //Message validated
                //Send data to the Physical Asset Adapter
                eventBus.request(PhysicalAssetAdapter.PA_SHADOWING_RELATIONSHIPS, jsonObj, ar -> {
                    if (ar.succeeded()) {
                        //Reply Physical Asset Adapter request's results to the adapter.
                        msg.reply(ar.result().body());
                    }
                });
            } //else nothing, message discarded.
        });
    }

    private boolean checkValidity(final JsonObject jsonObj) {
        //Method created for extendibility purposes.
        return jsonObj.containsKey(DataHub.DATA_HUB_NEW_DATA_DT_TYPE_LABEL)
                && jsonObj.containsKey(DataHub.DATA_HUB_NEW_DATA_DT_ID);
    }

    private boolean checkRelValidity(final JsonObject jsonObj) {
        //Method created for extendibility purposes.
        return jsonObj.containsKey(DataHub.DATA_HUB_NEW_DATA_DT_REL_NAME)
                && jsonObj.containsKey(DataHub.DATA_HUB_NEW_DATA_DT_TARGET)
                && jsonObj.containsKey(DataHub.DATA_HUB_NEW_DATA_DT_ID);
    }
}
