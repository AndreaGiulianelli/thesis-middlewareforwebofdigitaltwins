package verticles.worldadapter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.NetClient;
import io.vertx.core.net.NetSocket;

public class CoreEventDispatcher extends AbstractVerticle {
    /**
     * Event bus address where verticle send new event data to send to the Web Of Digital Twins Core.
     */
    public static final String NEW_EVENT = "events.interal.new";
    /**
     * Label for type in event body sent to Core Event Dispatcher.
     */
    public static final String TYPE_LABEL = "type";
    /**
     * Label for id in event body sent to Core Event Dispatcher.
     */
    public static final String ID_LABEL = "id";
    /**
     * Label for fidelity in event body sent to Core Event Dispatcher.
     */
    public static final String FIDELITY_LABEL = "fidelity";
    /**
     * Label for validity in event body sent to Core Event Dispatcher.
     */
    public static final String VALID_LABEL = "valid";
    /**
     * Label to specify the org id in event body.
     */
    public static final String ORG_LABEL = "org";
    /**
     * Label to specify the classes of the Digital Twin in event body.
     */
    public static final String CLASSES_LABEL = "classes";

    private static final int TCP_SOCKET_PORT = 1234;

    @Override
    public final void start() {
        final EventBus eventBus = vertx.eventBus();

        //Create socket
        NetClient client = vertx.createNetClient();
        client.connect(CoreEventDispatcher.TCP_SOCKET_PORT, "localhost", res -> {
           if (res.succeeded()) {
               System.out.println("Connected to CORE");

               // setup socket handler for incoming msg
               NetSocket socket = res.result();
               socket.handler(buffer -> {
                   final JsonObject jsonObj = buffer.toJsonObject();
                   //System.out.println("WoDT NODE CORE EVENT DISPATCHER - received msg - " + jsonObj.encode());
                   final InternalEventType type = InternalEventType.valueOf(jsonObj.getString(CoreEventDispatcher.TYPE_LABEL));
                   if (type == InternalEventType.VALIDITY) {
                       eventBus.send(ApiComponent.EVENT_BUS_DT_VALIDITY, jsonObj);
                   }
               });

               //Socket connected successfully, so listen to event bus events.
               eventBus.consumer(CoreEventDispatcher.NEW_EVENT, message -> {
                   System.out.println("CoreEventDispatcher, new event: " + message.body().toString());
                   final JsonObject event = new JsonObject(message.body().toString());
                   event.put(CoreEventDispatcher.ORG_LABEL, ApiComponent.URI_PREFIX);
                   switch (InternalEventType.valueOf(event.getString(CoreEventDispatcher.TYPE_LABEL))) {
                       case CREATE:
                           System.out.println("create");
                           final String createdId = event.getString(CoreEventDispatcher.ID_LABEL);
                           final int fidelity = event.getInteger(CoreEventDispatcher.FIDELITY_LABEL, -1);
                           System.out.println(createdId + " " + fidelity);
                           //Send event to socket
                           socket.write(event.encode());
                           break;
                       case DELETE:
                           System.out.println("delete");
                           final String deletedId = event.getString(CoreEventDispatcher.ID_LABEL);
                           //Send event to socket
                           socket.write(event.encode());
                           break;
                       case UPDATE:
                           System.out.println("update");
                           final String updatedId = event.getString(CoreEventDispatcher.ID_LABEL);
                           //Send event to socket
                           socket.write(event.encode());
                           break;
                       default:
                           System.out.println("bad event");
                           break;
                   }
               });
           } else {
               System.out.println("Error in connection to CORE");
           }
        });
    }
}
