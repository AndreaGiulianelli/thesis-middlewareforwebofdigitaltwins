package verticles.worldadapter;

public enum ObserveEventType {
    /**
     * Digital Twin created event. 
     */
    CREATE_TWIN("DigitalTwin.created"),
    /**
     * Digital Twin deleted event.
     */
    DELETE_TWIN("DigitalTwin.deleted"),
    /**
     * Digital Twin updated event.
     */
    UPDATE_TWIN("DigitalTwin.updated"),
    /**
     * Relationship created event.
     */
    CREATE_RELATIONSHIP("Relationship.created"),
    /**
     * Relationship deleted event.
     */
    DELETE_RELATIONSHIP("Relationship.deleted");

    private String stringRepresentation;

    ObserveEventType(final String repr) {
        this.stringRepresentation = repr;
    }

    @Override
    public String toString() {
        return this.stringRepresentation;
    }

    public static ObserveEventType fromAdtStringType(final String adtEventType) {
        ObserveEventType returnedType = null;
        switch (adtEventType) {
            case "Microsoft.DigitalTwins.Twin.Update":
                returnedType = ObserveEventType.UPDATE_TWIN;
                break;
            case "Microsoft.DigitalTwins.Twin.Create":
                returnedType = ObserveEventType.CREATE_TWIN;
                break;
            case "Microsoft.DigitalTwins.Twin.Delete":
                returnedType = ObserveEventType.DELETE_TWIN;
                break;
            case "Microsoft.DigitalTwins.Relationship.Create":
                returnedType = ObserveEventType.CREATE_RELATIONSHIP;
                break;
            case "Microsoft.DigitalTwins.Relationship.Delete":
                returnedType = ObserveEventType.DELETE_RELATIONSHIP;
                break;
            default:
                throw new IllegalArgumentException();
        }
        return returnedType;
    }
}
