package verticles.worldadapter;

import java.util.ArrayList;
import java.util.List;

import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;

public class EventObserverClient extends AbstractVerticle {

    /**
     * Event bus adress prefix in order to receive events from Twin Builder.
     * To complete the address concatenate the id of the twin to observe.
     */
    public static final String EVENT_OBSERVER_PREFIX = "eventobserver.newevent.";
    private static final String SIGNALR_NEGOTIATE_FUNCTION_URL = "https://eventgridsignalrfunctionapp.azurewebsites.net/api";

    private static final String LABEL_ID = "id";
    private static final String LABEL_SOURCE_ID = "sourceId";
    private static final String LABEL_TARGET_ID = "targetId";
    private static final String LABEL_TYPE = "type";
    private static final String LABEL_REL_NAME = "relationshipName";
    private static final String LABEL_PATCH_NAME = "patch";
    private static final String LABEL_DATETIME_NAME = "dateTime";

    @Override
    public final void start() {
        final EventBus eventBus = vertx.eventBus();
        final HubConnection hubConnection = HubConnectionBuilder
                .create(EventObserverClient.SIGNALR_NEGOTIATE_FUNCTION_URL)
                .build();

        //After building and before start the connection, define the hub listeners
        hubConnection.on("newMessage", (message) -> {
            System.out.println("New Message from SignalR: " + message);
            this.getId(message).forEach((id) -> {
                eventBus.publish(EventObserverClient.EVENT_OBSERVER_PREFIX + id, this.getEventObjFromAdtEvent(new JsonObject(message)).encode());
            });
        }, String.class);

        hubConnection.start().doOnComplete(() -> {
            System.out.println("Connected to SignalR!");
        }).doOnError((e) -> {
            System.out.println("Error SignalR: " + e);
        }).subscribe();
    }

    /*
     * Get the ids interested in the notification.
     * In case of a relationship there are two ids.
     */
    private List<String> getId(final String message) {
        List<String> ids = new ArrayList<>();
        JsonObject json = new JsonObject(message);
        if (json.containsKey("EventType")) {
            switch (ObserveEventType.fromAdtStringType(json.getString("EventType"))) {
                case DELETE_RELATIONSHIP:
                case CREATE_RELATIONSHIP:
                    //It's a relationship event -> there are two ids: the source and the target.
                    JsonObject body = json.getJsonObject("data");
                    ids.add(body.getString("$sourceId"));
                    ids.add(body.getString("$targetId"));
                    break;
                default:
                    //Normal notification: create, update, delete -> there is only one id.
                    if (json.containsKey("Id")) {
                        ids.add(json.getString("Id"));
                    }
                    break;
            }
        } else {
            System.out.println("Not specified EventType");
        }
        return ids;
    }

    private JsonObject getEventObjFromAdtEvent(final JsonObject adtEvent) {
        final JsonObject result = new JsonObject();
        final ObserveEventType adtEventType = ObserveEventType.fromAdtStringType(adtEvent.getString("EventType")); 
        switch (adtEventType) {
            case CREATE_TWIN:
                result.put(EventObserverClient.LABEL_ID, adtEvent.getString("Id"));
                result.put(EventObserverClient.LABEL_TYPE, ObserveEventType.CREATE_TWIN.toString());
                //result.put(EventObserverClient.LABEL_DATETIME_NAME, adtEvent.getString("EventDateTime"));
                break;
            case DELETE_TWIN:
                result.put(EventObserverClient.LABEL_ID, adtEvent.getString("Id"));
                result.put(EventObserverClient.LABEL_TYPE, ObserveEventType.DELETE_TWIN.toString());
                //result.put(EventObserverClient.LABEL_DATETIME_NAME, adtEvent.getString("EventDateTime"));
                break;
            case UPDATE_TWIN:
                result.put(EventObserverClient.LABEL_ID, adtEvent.getString("Id"));
                result.put(EventObserverClient.LABEL_TYPE, ObserveEventType.UPDATE_TWIN.toString());
                //result.put(EventObserverClient.LABEL_DATETIME_NAME, adtEvent.getString("EventDateTime"));
                result.put(EventObserverClient.LABEL_PATCH_NAME, adtEvent.getJsonObject("data").getJsonArray("patch").encode());
                break;
            case CREATE_RELATIONSHIP:
                result.put(EventObserverClient.LABEL_SOURCE_ID, adtEvent.getJsonObject("data").getString("$sourceId"));
                result.put(EventObserverClient.LABEL_TARGET_ID, adtEvent.getJsonObject("data").getString("$targetId"));
                result.put(EventObserverClient.LABEL_REL_NAME, adtEvent.getJsonObject("data").getString("$relationshipName"));
                result.put(EventObserverClient.LABEL_TYPE, ObserveEventType.CREATE_RELATIONSHIP.toString());
                //result.put(EventObserverClient.LABEL_DATETIME_NAME, adtEvent.getString("EventDateTime"));
                break;
            case DELETE_RELATIONSHIP:
                result.put(EventObserverClient.LABEL_SOURCE_ID, adtEvent.getJsonObject("data").getString("$sourceId"));
                result.put(EventObserverClient.LABEL_TARGET_ID, adtEvent.getJsonObject("data").getString("$targetId"));
                result.put(EventObserverClient.LABEL_REL_NAME, adtEvent.getJsonObject("data").getString("$relationshipName"));
                result.put(EventObserverClient.LABEL_TYPE, ObserveEventType.DELETE_RELATIONSHIP.toString());
                //result.put(EventObserverClient.LABEL_DATETIME_NAME, adtEvent.getString("EventDateTime"));
                break;
            default:
                break;
        }
        return result;
    }
}
