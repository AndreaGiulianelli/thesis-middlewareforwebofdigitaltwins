package verticles.worldadapter;

import java.util.HashMap;
import java.util.Map;

import com.azure.digitaltwins.core.implementation.models.ErrorResponseException;

import adtcrud.ADTAsyncCrud;
import adtcrud.TwinBuilderAsyncCrud;
import converter.JSONToRDF;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import model.DigitalTwin;
import model.utility.ModelConverter;
import utils.HttpCode;

public class ApiComponent extends AbstractVerticle {

    /**
     * Prefix for Digital Twin URI.
     */
    public static final String URI_PREFIX = "http://localhost:8080/api/twins/";
    /**
     * Key for the id of a Digital Twin in JSON body.
     */
    public static final String API_COMPONENT_DT_ID = "DtId";
    /**
     * Key for the fidelity of a Digital Twin in JSON body.
     */
    public static final String API_COMPONENT_DT_FIDELITY = "fidelity";
    /**
     * Event Bus address that CoreEventDispatcher use for communicate the validity of a DT. **Sperimental**
     */
    public static final String EVENT_BUS_DT_VALIDITY = "dt.validity";

    private static final int PORT = 8080;

    //Credentials to access the Azure Digital Twins instace
    private static final String TENANT_ID = "e99647dc-1b08-454a-bf8c-699181b389ab";
    private static final String APP_ID = "b3719ec9-1909-4e6e-bacb-c2ec4fb8a310";
    private static final String APP_SECRET = "yQR9AXFDyuo0.faopFHfT5LB.tHiwj4twa";
    private static final String HOSTNAME = "https://Org1-TwinBuilder.api.neu.digitaltwins.azure.net/";

    //Map where store validity of Digital Twins, in terms of fidelity respect.
    private Map<String, Boolean> validityMap = new HashMap<>();

    private TwinBuilderAsyncCrud twinBuilderCrud;

    @Override
    public final void start() {
        final EventBus eventBus = vertx.eventBus();
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        //Get Digital Twin API
        router.get("/api/twins/:dtId").handler(this::getDigitalTwin);
        //Create Digital Twin API
        router.post("/api/twins/:type/").handler(this::createDigitalTwin);
        //Delete Digital Twin API
        router.delete("/api/twins/:dtId").handler(this::deleteDigitalTwin);
        //Query Digital Twin Graph API
        router.post("/api/twins/sparql").handler(this::queryGraph);
        //Get Digital Twin Events API
        router.get("/api/twins/:dtId/events").handler(this::getDigitalTwinEvents);

        //Set-up twinBuilderCrud and connect
        this.twinBuilderCrud = new ADTAsyncCrud();
        //Wait the connection to ADT and then start the API service
        this.connectToTwinBuilder().onComplete(ar -> {
            if (ar.succeeded()) {
                //EWA connected successfully to the Azure Digital Twins instance
                System.out.println("External World Adapter connected to ADT");
                //Start the http server
                vertx.createHttpServer()
                    .requestHandler(router)
                    .listen(ApiComponent.PORT);
               System.out.println("Http Server on");
            }
        });

        eventBus.consumer(ApiComponent.EVENT_BUS_DT_VALIDITY, message -> {
            final JsonObject jsonObj = new JsonObject(message.body().toString());
            final String id = jsonObj.getString(CoreEventDispatcher.ID_LABEL);
            final boolean validity = jsonObj.getBoolean(CoreEventDispatcher.VALID_LABEL);
            this.validityMap.put(id, validity);
            System.out.println(id + " - " + (validity ? "VALID" : "NOT VALID"));
        });
    }

    private void getDigitalTwinEvents(final RoutingContext routingContext) {
        System.out.println("event request");
        HttpServerResponse response = routingContext.response();
        final EventBus eventBus = vertx.eventBus();
        final String dtId = routingContext.pathParam("dtId");
        //Create the web socket from the original request
        routingContext.request().toWebSocket()
        .onSuccess(ws -> {
            System.out.println("WebSocket created for Digital Twin: " + dtId);
            //Consume event for this Digital Twin id.
            eventBus.consumer(EventObserverClient.EVENT_OBSERVER_PREFIX + dtId, data -> {
                //Write data to web socket
                ws.writeTextMessage(data.body().toString());
            });
        }).onFailure((e) -> {
            response.setStatusCode(HttpCode.BAD_REQUEST.getCode()).end();
        });
    }

    private void queryGraph(final RoutingContext routingContext) {
        System.out.println("query request");
        HttpServerResponse response = routingContext.response();
        final String sparqlQuery = routingContext.getBodyAsString();
        this.twinBuilderCrud.query(sparqlQuery).onSuccess(res -> {
            response.putHeader("content-type", "application/sparql-results+json")
            .setStatusCode(HttpCode.OK.getCode())
            .end(res.encode());
        }).onFailure(err -> {
            response.setStatusCode(HttpCode.SERVER_ERROR.getCode())
            .end();
        });
    }

    private void deleteDigitalTwin(final RoutingContext routingContext) {
        System.out.println("delete request");
        final EventBus eventBus = vertx.eventBus();
        HttpServerResponse response = routingContext.response();
        final String dtId = routingContext.pathParam("dtId");
        this.twinBuilderCrud.deleteTwin(dtId).onSuccess(res -> {
           response.setStatusCode(HttpCode.NO_CONTENT.getCode())
           .end();

           //Inform the Core Event Dispatcher
           final JsonObject event = new JsonObject();
           event.put(CoreEventDispatcher.TYPE_LABEL, InternalEventType.DELETE)
           .put(CoreEventDispatcher.ID_LABEL, dtId);
           eventBus.send(CoreEventDispatcher.NEW_EVENT, event.encode());
        }).onFailure(err -> {
            if (this.isDigitalTwinNotFoundException(err)) {
                JsonObject returnedError = new JsonObject();
                returnedError.put("error", "Digital Twin Id not found.");
                response.putHeader("content-type", "application/json")
                        .setStatusCode(HttpCode.NOT_FOUND.getCode())
                        .end(returnedError.encode());
            } else {
                response.setStatusCode(HttpCode.SERVER_ERROR.getCode())
                .end();
            }
        });
    }

    private void createDigitalTwin(final RoutingContext routingContext) {
        System.out.println("post request");
        HttpServerResponse response = routingContext.response();
        JsonObject jsonBody;
        final JsonObject responseObj = new JsonObject();
        //Check validity of the request
        try {
            jsonBody = routingContext.getBodyAsJson();
            if (jsonBody == null) {
                //Body not in json format
                responseObj.put("error", "body need to be in JSON format.");
                response.putHeader("content-type", "application/json")
                .setStatusCode(HttpCode.BAD_REQUEST.getCode())
                .end(responseObj.encode());
                return;
            } else if (!jsonBody.containsKey(ApiComponent.API_COMPONENT_DT_ID)) {
                //Digital Twin id not specified
                responseObj.put("error", "specify Digital Twin Id and optionally fidelity in a JSON object");
                response.putHeader("content-type", "application/json")
                .setStatusCode(HttpCode.BAD_REQUEST.getCode())
                .end(responseObj.encode());
                return;
            }
        } catch (DecodeException e) {
            responseObj.put("error", "body need to be in JSON format.");
            response.putHeader("content-type", "application/json")
            .setStatusCode(HttpCode.BAD_REQUEST.getCode())
            .end(responseObj.encode());
            return;
        } catch (Exception e) { 
            System.out.println(e);
            //Internal server error
            response.putHeader("content-type", "application/json")
            .setStatusCode(HttpCode.SERVER_ERROR.getCode())
            .end(responseObj.encode());
            return;
        }
        //The body is OK, request the creation
        final EventBus eventBus = vertx.eventBus();
        //Get the type
        final String modelType = routingContext.pathParam("type");
        //Get the Digital Twin obj according to the type (internally has the model id for the creation)
        final DigitalTwin digitalTwin = ModelConverter.fromType(modelType);
        //Get the id specified in the json body
        final String dtId = jsonBody.getString(ApiComponent.API_COMPONENT_DT_ID);
        digitalTwin.setId(dtId);
        //Check if the digital twin already exists
        this.twinBuilderCrud.getDigitalTwin(digitalTwin.getId(), String.class).onFailure(err -> {
            if (this.isDigitalTwinNotFoundException(err)) {
                //Ok Digital Twin not exist, let's create one.
                this.twinBuilderCrud.createDigitalTwin(digitalTwin).onSuccess(res -> {
                    response.setStatusCode(HttpCode.CREATED.getCode())
                    .putHeader("Location", ApiComponent.URI_PREFIX + res)
                    .end();

                    //Inform the Core Event Dispatcher
                    final JsonObject event = new JsonObject();
                    event.put(CoreEventDispatcher.TYPE_LABEL, InternalEventType.CREATE)
                    .put(CoreEventDispatcher.ID_LABEL, dtId);
                    if (jsonBody.containsKey(ApiComponent.API_COMPONENT_DT_FIDELITY)) {
                        try {
                            int fidelity = jsonBody.getInteger(ApiComponent.API_COMPONENT_DT_FIDELITY);
                            event.put(CoreEventDispatcher.FIDELITY_LABEL, fidelity);
                            event.put(CoreEventDispatcher.CLASSES_LABEL, new JsonArray(digitalTwin.getClasses()));
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                    }
                    eventBus.send(CoreEventDispatcher.NEW_EVENT, event.encode());
                }).onFailure(e -> {
                    System.out.println(e);
                    response.setStatusCode(HttpCode.SERVER_ERROR.getCode())
                    .end();
                });
            } else {
                response.putHeader("content-type", "application/json")
                .setStatusCode(HttpCode.SERVER_ERROR.getCode())
                .end();
            }
        }).onSuccess(res -> {
            //Digital Twin already exist, return conflict with the existing one.
            responseObj.put("error", "Digital Twin with that ID already exist, check out in the Location header of the response to get it.");
            response.putHeader("content-type", "application/json")
            .setStatusCode(HttpCode.CONFLICT.getCode())
            .end(responseObj.encode());
        });
    }

    private void getDigitalTwin(final RoutingContext routingContext) {
        System.out.println("get request");
        HttpServerResponse response = routingContext.response();
        final String dtId = routingContext.pathParam("dtId");

        this.twinBuilderCrud.getDigitalTwin(dtId, String.class).onSuccess(stringResult -> {
            final JsonObject additionalProps = new JsonObject();
            additionalProps.put("inSynch", this.validityMap.getOrDefault(dtId, true));
            //Convert to RDF
            final String rdfResult = JSONToRDF.jsonGetDTResponseToRDF(stringResult, additionalProps);
            response.putHeader("content-type", "text/plain")
            .setStatusCode(HttpCode.OK.getCode()).end(rdfResult);
        }).onFailure(err -> {
            //Understand failure
            final JsonObject returnedError = new JsonObject();
            if (this.isDigitalTwinNotFoundException(err)) {
                returnedError.put("error", "Digital Twin Id not found.");
                response.putHeader("content-type", "application/json")
                        .setStatusCode(HttpCode.NOT_FOUND.getCode())
                        .end(returnedError.encode());
            } else {
                response.putHeader("content-type", "application/json")
                .setStatusCode(HttpCode.SERVER_ERROR.getCode())
                .end(returnedError.encode());
            }
        });
    }

    private boolean isDigitalTwinNotFoundException(final Throwable e) {
        if (e instanceof ErrorResponseException) {
            try {
                //I have to do this because Microsoft return a string mixed with json, and the error type is in the json part.
                JsonObject error = new JsonObject(e.getMessage().substring(e.getMessage().indexOf("\"") + 1, e.getMessage().length() - 1));
                final String errorCode = error.getJsonObject("error").getString("code");
                if (errorCode.equals("DigitalTwinNotFound")) {
                    //Digital Twins not found, report error
                    return true;
                }
            } catch (Exception ex) {
                return false;
            }
        }
        return false;
    }

    private Future<Void> connectToTwinBuilder() {
        final Promise<Void> isConnectedToADT = Promise.promise();
        vertx.executeBlocking(promise -> {
            try {
                //Connect to Azure Digital Twins instance
                this.twinBuilderCrud.connect(ApiComponent.TENANT_ID, ApiComponent.APP_ID, ApiComponent.APP_SECRET, ApiComponent.HOSTNAME);
                promise.complete();
            } catch (Exception e) {
                promise.fail(e);
            }
        }, res -> {
            if (res.succeeded()) {
               isConnectedToADT.complete(); 
            } else {
                System.out.println("(EWA) Problems in connecting to Azure Digital Twins instance");
            }
        });
        return isConnectedToADT.future();
    }
}
