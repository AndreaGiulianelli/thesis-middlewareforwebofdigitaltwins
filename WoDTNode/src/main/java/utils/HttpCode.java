package utils;

import java.util.HashMap;
import java.util.Map;

public enum HttpCode {
    /**
     * OK status code.
     */
    OK(200),
    /**
     * CREATED status code.
     */
    CREATED(201),
    /**
     * NO CONTENT status code.
     */
    NO_CONTENT(204),
    /**
     * BAD REQUEST status code.
     */
    BAD_REQUEST(400),
    /**
     * NOT FOUND status code.
     */
    NOT_FOUND(404),
    /**
     * CONFLICT status code.
     */
    CONFLICT(409),
    /**
     * SERVER ERROR status code.
     */
    SERVER_ERROR(500);

    private int statusCode;

    HttpCode(final int statusCode) {
        this.statusCode = statusCode;
    }

    public int getCode() {
        return this.statusCode;
    }

    public static HttpCode fromCode(final int code) {
        return MAP.get(code);
    }

    private static final Map<Integer, HttpCode> MAP = new HashMap<>();
    static {
        for (HttpCode code : HttpCode.values()) {
            MAP.put(code.statusCode, code);
        }
    }
}
