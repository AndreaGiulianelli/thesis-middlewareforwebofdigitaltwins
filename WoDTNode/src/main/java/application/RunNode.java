package application;

import io.vertx.core.Vertx;
import verticles.physicaladapter.PhysicalAssetAdapter;
import verticles.physicaladapter.datahubadapter.DataHub;
import verticles.worldadapter.ApiComponent;
import verticles.worldadapter.CoreEventDispatcher;
import verticles.worldadapter.EventObserverClient;

/**
 * Start point for the Service.
 *
 */
public final class RunNode {

    private RunNode() { }
    /**
     * @param strings params
     */
    public static void main(final String...strings) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new CoreEventDispatcher());
        vertx.deployVerticle(new DataHub());
        vertx.deployVerticle(new PhysicalAssetAdapter());
        vertx.deployVerticle(new ApiComponent());
        vertx.deployVerticle(new EventObserverClient());
    }
}
