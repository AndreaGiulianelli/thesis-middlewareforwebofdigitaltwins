package converter.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Element {
    private String name;
    private List<Role> roles;
    private String subjectAlias;
    private boolean questionWord;
    private Optional<String> dataType;

    public Element(final String name) {
        this.name = name;
        this.roles = new ArrayList<>();
        this.questionWord = false;
        this.dataType = Optional.empty();
        this.subjectAlias = "T";
    }

    public void setSubjectAlias(final String subjectAlias) {
        this.subjectAlias = subjectAlias;
    }
    
    public void setQuestionWord(final boolean state) {
        this.questionWord = state;
    }
    
    public void setDataTypeString(final String string) {
        this.dataType = Optional.ofNullable(string);
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getSubjectAlias() {
        return this.subjectAlias;
    }
    
    public String getName() {
        return this.name;
    }
    
    public Optional<String> getDataTypeString() {
        return this.dataType;
    }
   
    /*
     * It's not a safe copy, you can modify this. (ANTIPATTERN, modify in the final release)
     */
    public List<Role> getRoles() {
        return this.roles;
    }
    
    public boolean isQuestionWord() {
        return this.questionWord;
    }

    @Override
    public String toString() {
        return "Element [name=" + name + ", roles=" + roles + ", subjectAlias=" + subjectAlias + ", questionWord=" + questionWord
                + "]";
    }

}
