package converter.model;

public enum Role {
    SUBJECT,
    PREDICATE,
    OBJECT;
}
