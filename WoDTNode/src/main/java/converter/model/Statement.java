package converter.model;

public class Statement {
    private Element subject;
    private Predicate predicate;
    private Element object;
    
    public Statement() {
    }
    
    public void addSubject(final Element element) {
        this.subject = element;
    }
    
    public void addPredicate(final Predicate element) {
        this.predicate = element;
    }
    
    public void addObject(final Element element) {
        this.object = element;
    }
   
    public Element getSubject() {
        return this.subject;
    }

    public Predicate getPredicate() {
        return this.predicate;
    }

    public Element getObject() {
        return this.object;
    }    

    @Override
    public String toString() {
        return "Statement [subject=" + subject + ", predicate=" + predicate + ", object=" + object + "]";
    }
    
    
}
