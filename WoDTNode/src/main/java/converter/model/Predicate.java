package converter.model;

public class Predicate {
    private String name;
    private PredicateType type;
    
    public Predicate(final String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setType(final PredicateType type) {
        this.type = type;
    }
    
    public PredicateType getType() {
        return this.type;
    }

    @Override
    public String toString() {
        return "Predicate [name=" + name + ", type=" + type + "]";
    }
}
