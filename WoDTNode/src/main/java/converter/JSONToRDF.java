package converter;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFWriter;

import io.vertx.core.json.JsonObject;
import verticles.worldadapter.ApiComponent;

public final class JSONToRDF {

    private static final String PREFIX = ApiComponent.URI_PREFIX; 
    private static final String PREFIX_PROP = "http://example.com/"; 
    private static final String PREFIX_INTERNAL_PROP = "http://localhost:8080/properties#"; 

    private JSONToRDF() { }
    /**
     * Turn Azure Digital Twins SDK getDigitalTwin response in RDF format.
     * @param jsonString - the jsonString returned from Azure Digital Twins SDK.
     * @param additionalInternalProp - json object that contains all the internal propreties to include (for example the validity)
     * @return the same informations (the important ones) in RDF format.
     */
    public static String jsonGetDTResponseToRDF(final String jsonString, final JsonObject additionalInternalProp) {
        JsonObject jsonObj = new JsonObject(jsonString);
        final String dtId = jsonObj.getString("$dtId");
        //Remove unused stuff
        jsonObj.remove("$dtId");
        jsonObj.remove("$metadata");
        jsonObj.remove("$etag");

        //Now all the informations are Digital Twins property, transform it in RDF format
        Model model = ModelFactory.createDefaultModel();
        Resource digitalTwinResource = model.createResource(JSONToRDF.PREFIX + dtId);
        //Add Digital Twins property
        jsonObj.iterator().forEachRemaining((entry) -> {
            if (!isJsonObject(entry.getValue().toString())) {
                final Property prop = model.createProperty(JSONToRDF.PREFIX_PROP + entry.getKey());
                digitalTwinResource.addLiteral(prop, entry.getValue());
            } else {
                final JsonObject propertyValueObj = new JsonObject(entry.getValue().toString());
                propertyValueObj.iterator().forEachRemaining(entryProp -> {
                    final Property prop = model.createProperty(JSONToRDF.PREFIX_PROP + entryProp.getKey());
                    digitalTwinResource.addLiteral(prop, entryProp.getValue());
                });
            }
        });
        //Add additional internal property
        additionalInternalProp.iterator().forEachRemaining((entry) -> {
            final Property prop = model.createProperty(JSONToRDF.PREFIX_INTERNAL_PROP + entry.getKey());
            digitalTwinResource.addLiteral(prop, entry.getValue());
        });
        model.setNsPrefix("ex", JSONToRDF.PREFIX_PROP);
        model.setNsPrefix("adtprops", JSONToRDF.PREFIX_INTERNAL_PROP);
        model.setNsPrefix("adt", JSONToRDF.PREFIX);
        return RDFWriter.create()
                .lang(Lang.TTL)
                .source(model).asString();
    }

    private static boolean isJsonObject(final String obj) {
        try {
            new JsonObject(obj);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
