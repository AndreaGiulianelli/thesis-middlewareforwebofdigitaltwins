package converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import converter.model.Element;
import converter.model.Predicate;
import converter.model.PredicateType;
import converter.model.Role;
import converter.model.Statement;

/**
 * Simple converter for Sparql query in Azure Digital Twins SQL.
 * At the moment support only easy query, with relationships.
 * NO FILTER and NO OPTIONAL SUPPORT.
 * Submit query with filter or optional will ruin the algorithm.
 */
public class SparqlToSqlConverter {

    //List of all statements in sparql query
    private List<Statement> statements;

    //List of all variabile in the select
    private List<String> sqlSelect;
    //List of all statements in sparql query in String format
    private List<String> sqlWhere;

    private Map<String, Element> elementMap;

    private StringBuilder select;
    private StringBuilder from;
    private StringBuilder where;

    public SparqlToSqlConverter() {
        this.statements = new ArrayList<>();
        this.sqlSelect = new ArrayList<>();
        this.sqlWhere = new ArrayList<>();
        this.elementMap = new HashMap<>();
        this.select = new StringBuilder();
        this.select.append("SELECT");
        this.from = new StringBuilder();
        this.from.append("FROM DIGITALTWINS T");
        this.where = new StringBuilder();
        this.where.append("WHERE ");
    }

    /**
     * @param sparqlQuery
     * @return the query in Azure Digital Twins SQL format. 
     */
    public String convert(final String sparqlQuery) {
        System.out.println("Sparql Query: " + sparqlQuery + "\n");
        /*
         * In the final version it can be done some preprocessing in order
         * to work also with lowercase select, where and filter without problems.
         */
        //-------------------------- STEP 1 -----------------------------------
        String[] tmp1 = sparqlQuery.split("WHERE");
        //Get the select variables
        this.sqlSelect.addAll(Arrays.asList(tmp1[0].split("SELECT")[1].replace("?", "").split(" ")));
        //Debug
        this.explore(this.sqlSelect);
        //-------------------------- STEP 2 -----------------------------------
        tmp1[1] = tmp1[1].replace("{", "");
        tmp1[1] = tmp1[1].replace("}", "");
        this.sqlWhere.addAll(Arrays.asList(tmp1[1].split("\\.")));
        //Debug
        this.explore(this.sqlWhere);
        //-------------------------- STEP 3 -----------------------------------
        this.sqlWhere.forEach((statementText) -> {
            final Statement statement = new Statement();
            String[] tmp = statementText.strip().split(" ");
            //In case the object is a string and has a phrase with spaces.
            if (tmp.length > 3) {
                for (int i = 3; i < tmp.length; i++) {
                    tmp[2] += " " + tmp[i];
                }
            }

            if (tmp.length >= 3) {
                //Strip each element to eliminates leading and ending spaces.
                for (int i = 0; i < 3; i++) {
                    tmp[i] = tmp[i].strip();
                }

                Element subject;
                Predicate predicate;
                Element object;
                if (this.elementMap.containsKey(tmp[0])) {
                    subject = this.elementMap.get(tmp[0]);
                } else {
                    subject = new Element(tmp[0]);
                    this.elementMap.put(tmp[0], subject);
                }
                subject.getRoles().add(Role.SUBJECT);

                predicate = new Predicate(tmp[1]);

                if (this.elementMap.containsKey(tmp[2])) {
                    object = this.elementMap.get(tmp[2]);
                } else {
                    object = new Element(tmp[2]);
                    this.elementMap.put(tmp[2], object);
                }
                object.getRoles().add(Role.OBJECT);

                //Eliminate the datatypes and the "" around
                if (object.getName().contains("^^xsd:")) {
                    //Set datatype
                    object.setDataTypeString(object.getName().substring(object.getName().indexOf("^^xsd:") + 2));
                    //Set the right name/content
                    object.setName(object.getName().substring(1, object.getName().indexOf("^^xsd:") - 1));
                }

                //Check question words (N.B. Predicate can't be a question word -> for the moment)
                if (subject.getName().contains("?")) {
                    subject.setQuestionWord(true);
                }
                //Check that if ? appears, it isn't a string.
                if (object.getName().contains("?") && object.getDataTypeString().isEmpty()) {
                    object.setQuestionWord(true);
                }

                /*
                 * For now think only with CURIEs, in final version may be right to extend to extended syntax.
                 * 
                 * Check if the predicate is an ObjectProperty or a DataTypeProperty.
                 * Control if contains : -> mfg:Doctor is an URI -> so when there is a CURIEs
                 * Then also control if it isn't a string, in fact in a string the char : may appear.
                 */
                if (object.getName().contains(":") && object.getDataTypeString().isEmpty()) {
                    //It's an Object Property
                    predicate.setType(PredicateType.OBJECT);
                } else {
                    predicate.setType(PredicateType.DATA);
                }

                //Now add the elements and the predicate to the statement
                statement.addSubject(subject);
                statement.addPredicate(predicate);
                statement.addObject(object);

                //Now add the statement to the list
                this.statements.add(statement);
            }
        });

        this.explore(statements);
        //-------------------------- STEP 4 -----------------------------------
        //-------------------------- STEP 5 -----------------------------------
        //-------------------------- STEP 6 - 6bis - 7 -----------------------------------
        this.explore(this.elementMap.values());
        this.elementMap.values().stream().filter((element) -> element.isQuestionWord() && !element.getRoles().stream().allMatch((x) -> x == Role.OBJECT)).forEach((element) -> {
            if (element.getRoles().stream().allMatch((x) -> x == Role.SUBJECT)) { //case subject
                sqlSelect.forEach(System.out::println);
                if (sqlSelect.contains(element.getName().substring(1))) {
                    select.append(" T.$dtId AS " + element.getName().substring(1) + ", ");
                }
                element.setSubjectAlias("T");
            } else if (element.getRoles().contains(Role.SUBJECT) && element.getRoles().contains(Role.OBJECT)) { //case join
                //find relation
                Statement relStatement = statements.stream().filter((statement) -> statement.getObject().equals(element)).findFirst().get();
                element.setSubjectAlias(element.getName().substring(1));
                from.append("\nJOIN " + element.getSubjectAlias() + " RELATED " + relStatement.getSubject().getSubjectAlias() + "." + relStatement.getPredicate().getName().split(":")[1]);
                if (sqlSelect.contains(element.getName().substring(1))) {
                    //somewhere it's also subject (check the if) and also is a question word (check the filter) so insert in the select its dtId.
                    select.append(" " + element.getSubjectAlias() + ".$dtId AS " + element.getSubjectAlias() + ", ");
                }
            }
        });

        //Now that all the subject alias are set work on question word that serve only as objects -> case object
        this.elementMap.values().stream().filter((element) -> element.isQuestionWord() && element.getRoles().stream().allMatch((x) -> x == Role.OBJECT)).forEach((element) -> {
            Statement objStatement = statements.stream().filter((statement) -> statement.getObject().equals(element)).findFirst().get();
            select.append(" " + objStatement.getSubject().getSubjectAlias() + "." + objStatement.getPredicate().getName().split(":")[1] + " AS " + element.getName().substring(1) + ", ");
        });
        //-------------------------- STEP 8 -----------------------------------
        final List<String> whereComponent = new ArrayList<>();
        this.statements.forEach((statement) -> {
            final Element subject = statement.getSubject();
            final Predicate predicate = statement.getPredicate();
            final Element object = statement.getObject();

            if (!subject.isQuestionWord()) {
                final String text = subject.getSubjectAlias() + ".$dtId" + " = \'" + subject.getName().split(":")[1] + "\'";
                if (!whereComponent.contains(text)) {
                    where.append((whereComponent.size() > 0 ? " AND\n" : "") + text);
                    whereComponent.add(text);
                }
            } 
            if (!object.isQuestionWord()) {
                if (!object.getDataTypeString().isPresent() && object.getName().contains(":")) {
                    //It's an dtId of an object in relationship
                    final String joinAlias = predicate.getName().strip().split(":")[1]; //Use this name, because when the Object is an ID, it's alias computed by this algorithm is the same of the subject, so use the name of the predicate.
                    from.append("\nJOIN " + joinAlias + " RELATED " + object.getSubjectAlias() + "." + predicate.getName().strip().split(":")[1]);
                    final String text = joinAlias + ".$dtId = " + "\'" + object.getName().split(":")[1] + "\'";
                    where.append((whereComponent.size() > 0 ? " AND\n" : "") + text);
                    whereComponent.add(text);
                } else {
                    //final String text = subject.getSubjectAlias() + "." + predicate.getName().split(":")[1] + " = " + (object.getDataTypeString().isPresent() ? "\"" +  object.getName() + "\"^^" + object.getDataTypeString().get() : (object.getName().contains(":") ? ("\'" + object.getName().split(":")[1] + "\'") : object.getName()));
                    final String text = subject.getSubjectAlias() + "." + predicate.getName().split(":")[1] + " = " + (object.getDataTypeString().isPresent() ? "\"" +  object.getName() + "\"^^" + object.getDataTypeString().get() : object.getName());
                    if (!whereComponent.contains(text)) {
                        where.append((whereComponent.size() > 0 ? " AND\n" : "") + text);
                        whereComponent.add(text);
                    }
                }
            }
        });

        String selectString = select.toString();
        if (selectString.endsWith(", ")) {
            selectString = selectString.substring(0, selectString.length() - 2);
        }
        final String adtSqlQuery = selectString + "\n" + from.toString() + "\n" + where.toString();
        return adtSqlQuery;
    }

    private void explore(final Iterable<?> it) {
        it.forEach((x) -> System.out.println(x));
    }
}
