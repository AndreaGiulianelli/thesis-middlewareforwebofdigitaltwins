package converter;

import java.util.List;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public final class JSONToSparqlJSON {

    private JSONToSparqlJSON() { }

    public static JsonObject convert(final List<JsonObject> results) {
        final JsonObject returnedObj = new JsonObject();
        //Keys are question words, values are the values associated
        final JsonObject vars = new JsonObject();
        final JsonArray questionWords = new JsonArray();
        if (!results.isEmpty()) {
            //Add all the question words in a JsonArray, get the field names from the first result
            results.get(0).fieldNames().forEach(questionWord -> questionWords.add(questionWord));
            //Put the question words array in the vars object
            vars.put("vars", questionWords);
            //Put the vars object in the head field of the Sparql JSON response
            returnedObj.put("head", vars);
            final JsonArray bindings = new JsonArray();
            results.forEach(result -> {
                final JsonObject binding = new JsonObject();
                questionWords.forEach(qw -> {
                    binding.put((String) qw, new JsonObject().put("value", result.getValue((String) qw)));
                });
                bindings.add(binding);
            });
            returnedObj.put("results", new JsonObject().put("bindings", bindings));
        }
        return returnedObj;
    }
}
