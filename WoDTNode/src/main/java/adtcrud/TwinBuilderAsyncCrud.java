package adtcrud;

import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import model.DigitalTwin;

public interface TwinBuilderAsyncCrud extends TwinBuilderAsync {
    /**
     * @param <T>
     * @param digitalTwin - digital twin to create.
     * @return a future for Digital Twin creation returning the Id (specified in the input, but returned for confirm).
     */
    <T extends DigitalTwin> Future<String> createDigitalTwin(T digitalTwin);
    /**
     * @param <T>
     * @param id - id of the digital twin to retrieve
     * @param resultClass - class used to represent the response
     * @return a future for Digital Twin get.
     */
    <T> Future<T> getDigitalTwin(String id, Class<T> resultClass);
    /**
     * @param id - the id of the Digital Twin to delete.
     * @return a future for Digital Twin deletion.
     */
    Future<Void> deleteTwin(String id);
    /**
     * @param query - the query to run.
     * @return a future for the query, it return the result in the SPARQL JSON format.
     */
    Future<JsonObject> query(String query);
}
