package adtcrud;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.azure.digitaltwins.core.BasicDigitalTwin;
import com.azure.digitaltwins.core.BasicDigitalTwinMetadata;
import com.azure.digitaltwins.core.BasicRelationship;
import com.azure.digitaltwins.core.DigitalTwinsAsyncClient;
import com.azure.digitaltwins.core.DigitalTwinsClientBuilder;
import com.azure.digitaltwins.core.models.CreateOrReplaceRelationshipOptions;
import com.azure.identity.ClientSecretCredentialBuilder;

import converter.JSONToSparqlJSON;
import converter.SparqlToSqlConverter;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import model.DigitalTwin;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import utils.HttpCode;

public class ADTAsyncCrud implements TwinBuilderAsyncCrud, TwinBuilderAsyncShadower {

    private DigitalTwinsAsyncClient digitalTwinClient;

    public final void connect(final String tenantId, final String appId, final String appSecret, final String hostName) {
        this.digitalTwinClient = new DigitalTwinsClientBuilder()
                .credential(
                    new ClientSecretCredentialBuilder()
                        .tenantId(tenantId)
                        .clientId(appId)
                        .clientSecret(appSecret)
                        .build())
                .endpoint(hostName)
                .buildAsyncClient();
    }

    @Override
    public final <T extends DigitalTwin> Future<String> createDigitalTwin(final T digitalTwin) {
        final Promise<String> promise = Promise.promise();
        try {
            /*
             * create a basic digital twin because 
             * the SDK want an object and the class of that object.
             * So, if we want to apply the DIP principle we can't pass the object typed as 
             * DigitalTwin (get from the converter to not repeat code) and then the specific class.
             * So, create the basic Digital twin getting the info from the original digital twin obj.
             */
            BasicDigitalTwin basicTwin = new BasicDigitalTwin(digitalTwin.getId());
            basicTwin.setMetadata(new BasicDigitalTwinMetadata());
            basicTwin.getMetadata().setModelId(digitalTwin.getModel());
            this.digitalTwinClient.createOrReplaceDigitalTwin(digitalTwin.getId(), basicTwin, BasicDigitalTwin.class).doOnSuccess(res -> {
                promise.complete(digitalTwin.getId());
            }).doOnError(e -> {
                System.out.println(e);
                promise.fail(e);
            }).subscribe();
        } catch (Exception e) {
            System.out.println(e);
            promise.fail(e);
        }
        return promise.future();
    }

    @Override
    public final <T> Future<T> getDigitalTwin(final String id, final Class<T> resultClass) {
        final Promise<T> promise = Promise.promise();
        try {
            this.digitalTwinClient.getDigitalTwin(id, resultClass).doOnSuccess(res -> {
                promise.complete(res);
            }).doOnError((e) -> {
                //System.out.println("Error ADTCrud: " + e);
                promise.fail(e);
            }).subscribe();
        } catch (Exception e) {
            System.out.println("Error ADTCrud: " + e);
            promise.fail(e);
        }
        return promise.future();
    }

    @Override
    public final Future<Void> deleteTwin(final String id) {
        final Promise<Void> promise = Promise.promise();
        try {
            CompositeFuture.all(this.deleteIncomingRelationShips(id), this.deleteOutgoingRelationShips(id))
            .onSuccess(ar -> {
                System.out.println("Relationships deleted");
                this.digitalTwinClient.deleteDigitalTwin(id).doOnSuccess(res -> {
                    promise.complete();
                }).doOnError(err -> {
                    System.out.println("Error ADTCrud: " + err);
                    promise.fail(err);
                }).subscribe();
            }).onFailure(e -> {
                promise.fail(e);
            });
        } catch (Exception e) {
            System.out.println("Error ADTCrud: " + e);
            promise.fail(e);
        }
        return promise.future();
    }

    private Future<Void> deleteIncomingRelationShips(final String id) {
        final Promise<Void> promise = Promise.promise();
        List<Mono<Void>> delTasks = new ArrayList<>();
        try {
            this.digitalTwinClient.listIncomingRelationships(id)
            .doOnNext(incomingRel -> {
                //Add task to delete this relationship
                delTasks.add(this.digitalTwinClient.deleteRelationship(incomingRel.getSourceId(), incomingRel.getRelationshipId()).subscribeOn(Schedulers.boundedElastic()));
            })
            .doAfterTerminate(() -> {
                //When we start all the delete tasks, subscribe to them and when all completed we can complete the promise.
                if (!delTasks.isEmpty()) {
                    System.out.println("Deleted Incoming");
                    Mono.when(delTasks)
                        .doOnSuccess(res -> promise.complete())
                        .doOnError(err -> {
                            System.out.println(err);
                            promise.fail(err);
                        }).subscribe();
                } else {
                    System.out.println("Incoming rel empty");
                    promise.complete();
                }
            }).subscribe();
        } catch (Exception e) {
            System.out.println(e);
            promise.fail(e);
        }
        return promise.future();
    }
 
    private Future<Void> deleteOutgoingRelationShips(final String id) {
        final Promise<Void> promise = Promise.promise();
        List<Mono<Void>> delTasks = new ArrayList<>();
        try {
            this.digitalTwinClient.listRelationships(id, BasicRelationship.class)
            .doOnNext(outgoingRel -> {
                //Add task to delete this relationship
                delTasks.add(this.digitalTwinClient.deleteRelationship(id, outgoingRel.getId()).subscribeOn(Schedulers.boundedElastic()));
            }).doAfterTerminate(() -> {
                //When we start all the delete tasks, subscribe to them and when all completed we can complete the promise.
                if (!delTasks.isEmpty()) {
                    System.out.println("Deleted Outgoing");
                    Mono.when(delTasks)
                        .doOnSuccess(x -> promise.complete())
                        .doOnError(err -> {
                            System.out.println(err);
                            promise.fail(err);
                        }).subscribe();
                } else {
                    System.out.println("Outgoing rel empty");
                    promise.complete();
                }
            }).subscribe();
        } catch (Exception e) {
            System.out.println(e);
            promise.fail(e);
        }
        return promise.future();
    }

    @Override
    public final Future<JsonObject> query(final String query) {
        //Convert query from Sparql to Azure Digital Twin SQL language.
        SparqlToSqlConverter converter = new SparqlToSqlConverter();
        final String adtQuery = converter.convert(query);
        System.out.println(adtQuery);
        final List<JsonObject> results = new ArrayList<JsonObject>();
        final Promise<JsonObject> promise = Promise.promise();
        try {
            this.digitalTwinClient.query(adtQuery, String.class).doOnNext(resString -> {
                System.out.println(resString);
                results.add(new JsonObject(resString));
            }).doOnComplete(() -> {
                promise.complete(JSONToSparqlJSON.convert(results));
            }).doOnError(err -> {
                System.out.println(err);
                promise.fail(err);
            }).subscribe();
        } catch (Exception e) {
            System.out.println(e);
            promise.fail(e);
        }
        return promise.future();
    }

    @Override
    public final Future<Integer> createRelation(final String sourceId, final String targetId, final String relationName) {
        Promise<Integer> promise = Promise.promise();
        final String relationId = List.of(sourceId, relationName, targetId).stream().collect(Collectors.joining("-"));
        try {
            BasicRelationship rel = new BasicRelationship(relationId, sourceId, targetId, relationName);
            this.digitalTwinClient.createOrReplaceRelationshipWithResponse(sourceId, relationId, rel, BasicRelationship.class, new CreateOrReplaceRelationshipOptions())
            .doOnSuccess(res -> {
                promise.complete(HttpCode.OK.getCode());
            }).doOnError(err -> {
                promise.complete(Integer.valueOf(err.getMessage().split(",")[0].split(" ")[2]));
            })
            .subscribe();
        } catch (Exception e) {
            System.out.println(e);
            promise.fail(e);
        }
        return promise.future();
    }

    @Override
    public final Future<Void> updateDigitalTwin(final DigitalTwin digitalTwin) {
        Promise<Void> promise = Promise.promise();
        try {
            this.digitalTwinClient.updateDigitalTwin(digitalTwin.getId(), digitalTwin.getPatchDocument()).doOnSuccess(res -> {
                promise.complete();
            }).doOnError(err -> {
                promise.fail(err);
            }).subscribe();
        } catch (Exception e) {
            promise.fail(e);
        }
        return promise.future();
    }

}
