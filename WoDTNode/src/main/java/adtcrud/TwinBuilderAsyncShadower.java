package adtcrud;

import io.vertx.core.Future;
import model.DigitalTwin;

public interface TwinBuilderAsyncShadower extends TwinBuilderAsync {
    /**
     * @param sourceId - the id of the source of the relation
     * @param targetId - the id of the target of the relation
     * @param relationName - the name of the relation property in the target
     * @return a future in order to listen for completion, return status code.
     */
    Future<Integer> createRelation(String sourceId, String targetId, String relationName);
    /**
     * @param digitalTwin - digital twin to update
     * @return a future in order to listen for completion.
     */
    Future<Void> updateDigitalTwin(DigitalTwin digitalTwin);
}
