package adtcrud;

public interface TwinBuilderAsync {
    /**
     * @param tenantId .
     * @param appId .
     * @param appSecret .
     * @param hostName .
     */
    void connect(String tenantId, String appId, String appSecret, String hostName);
}
