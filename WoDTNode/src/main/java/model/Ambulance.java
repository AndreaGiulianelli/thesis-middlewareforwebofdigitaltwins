package model;

import java.util.List;

import com.azure.core.models.JsonPatchDocument;
import com.azure.digitaltwins.core.DigitalTwinPropertyMetadata;
import com.azure.digitaltwins.core.models.DigitalTwinsJsonPropertyNames;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.vertx.core.json.JsonObject;
import verticles.physicaladapter.datahubadapter.DataHub;

public class Ambulance extends AbstractDigitalTwin {
    @JsonProperty("fuelPercentage")
    private int fuelPercentage;
    @JsonProperty("busy")
    private boolean busy;
    @JsonProperty("latitude")
    private double latitude;
    @JsonProperty("longitude")
    private double longitude;

    @JsonProperty(DigitalTwinsJsonPropertyNames.DIGITAL_TWIN_METADATA)
    private AmbulanceMetadata metadata = new AmbulanceMetadata();

    /*
     * Json patch document that will be built during the set of the object.
     * In this way we can abstract the name used in the digital twin instance.
     */
    private transient JsonPatchDocument patchDocument = new JsonPatchDocument();

    public Ambulance() {
        this.metadata.setModelId("dtmi:com:contoso:ambulance;1");
    }

    /**
     * @return fuelPercentage
     */
    public int getFuelPercentage() {
        return this.fuelPercentage;
    }
    /**
     * @param fuelPercentage
     */
    public void setFuelPercentage(final int fuelPercentage) {
        this.fuelPercentage = fuelPercentage;
        this.patchDocument.appendAdd("/fuelPercentage", fuelPercentage);
    }

    /**
     * @return budy
     */
    public boolean isBusy() {
        return this.busy;
    }
    /**
     * @param busy
     */
    public void setBusy(final boolean busy) {
        this.busy = busy;
        this.patchDocument.appendAdd("/busy", busy);
    }

    /**
     * @return latitude
     */
    public double getLatitude() {
        return this.latitude;
    }
    /**
     * @param latitude
     */
    public void setLatitude(final double latitude) {
        this.latitude = latitude;
        this.patchDocument.appendAdd("/latitude", latitude);
    }

    /**
     * @return longitude
     */
    public double getLongitude() {
        return this.longitude;
    }
    /**
     * @param longitude
     */
    public void setLongitude(final double longitude) {
        this.longitude = longitude;
        this.patchDocument.appendAdd("/longitude", this.longitude);
    }

    /**
     * @return metadata
     */
    public AmbulanceMetadata getMetadata() {
        return this.metadata;
    }
    /**
     * @param metadata metadata of twin.
     */
    public void setMetadata(final AmbulanceMetadata metadata) {
        this.metadata = metadata;
    }

    /**
     * @param obj that contains parameters.
     */
    @Override
    public void setFromJsonObject(final JsonObject obj) {
        if (obj.containsKey(DataHub.DATA_HUB_NEW_DATA_DT_ID)) {
            this.setId(obj.getString(DataHub.DATA_HUB_NEW_DATA_DT_ID));
        }
        if (obj.containsKey("fuelPercentage")) {
            this.setFuelPercentage(obj.getInteger("fuelPercentage"));
        }
        if (obj.containsKey("busy")) {
            this.setBusy(obj.getBoolean("busy"));
        }
        if (obj.containsKey("latitude")) {
            this.setLatitude((Double) obj.getDouble("latitude"));
        }
        if (obj.containsKey("longitude")) {
            this.setLongitude((Double) obj.getDouble("longitude"));
        }
    }

    @Override
    public final JsonPatchDocument getPatchDocument() {
        return this.patchDocument;
    }

    @Override
    public final String getModel() {
        return this.metadata.getModelId();
    }

    @Override
    public final List<String> getClasses() {
        return List.of("healthcare", "city");
    }

    /**
     * It's necessary only for the Digital Twin serialization.
     */
    private static class AmbulanceMetadata extends DigitalTwinMetadata {
        @JsonProperty("fuelPercentage")
        private DigitalTwinPropertyMetadata fuelPercentage;
        @JsonProperty("busy")
        private DigitalTwinPropertyMetadata busy;
        @JsonProperty("lastLocation")
        private DigitalTwinPropertyMetadata lastLocation;

        /**
         * @return fuel percentage metadata.
         */
        public DigitalTwinPropertyMetadata getFuelPercentage() {
            return this.fuelPercentage;
        }
        /**
         * @param fuelPercentage metadata.
         */
        public void setFuelPercentage(final DigitalTwinPropertyMetadata fuelPercentage) {
            this.fuelPercentage = fuelPercentage;
        }

        /**
         * @return busy metadata.
         */
        public DigitalTwinPropertyMetadata getBusy() {
            return this.busy;
        }
        /**
         * @param fuelPercentage metadata.
         */
        public void setBusy(final DigitalTwinPropertyMetadata busy) {
            this.busy = busy;
        }

        /**
         * @return lastLocation metadata.
         */
        public DigitalTwinPropertyMetadata getLastLocation() {
            return this.lastLocation;
        }
        /**
         * @param lastLocation metadata.
         */
        public void setLastLocation(final DigitalTwinPropertyMetadata lastLocation) {
            this.lastLocation = lastLocation;
        }
    }

}
