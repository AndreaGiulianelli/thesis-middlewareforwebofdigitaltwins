package model;

import java.util.List;

import com.azure.core.models.JsonPatchDocument;
import com.azure.digitaltwins.core.DigitalTwinPropertyMetadata;
import com.azure.digitaltwins.core.models.DigitalTwinsJsonPropertyNames;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.vertx.core.json.JsonObject;
import verticles.physicaladapter.datahubadapter.DataHub;

public class Mission extends AbstractDigitalTwin {
    @JsonProperty("place")
    private String place;
    @JsonProperty("status")
    private int status;

    @JsonProperty(DigitalTwinsJsonPropertyNames.DIGITAL_TWIN_METADATA)
    private MissionMetadata metadata = new MissionMetadata();

    /*
     * Json patch document that will be built during the set of the object.
     * In this way we can abstract the name used in the digital twin instance.
     */
    private transient JsonPatchDocument patchDocument = new JsonPatchDocument();

    public Mission() {
        this.metadata.setModelId("dtmi:com:contoso:mission;1");
    }

    /**
     * @return place
     */
    public String getPlace() {
        return this.place;
    }

    /**
     * @param place
     */
    public void setPlace(final String place) {
        this.place = place;
        this.patchDocument.appendAdd("/place", place);
    }

    /**
     * @return status
     */
    public int getStatus() {
        return this.status;
    }

    /**
     * @param status
     */
    public void setStatus(final int status) {
        this.status = status;
        this.patchDocument.appendAdd("/status", status);
    }

    /**
     * @return status typed
     */
    public MissionStatus getMissionStatus() {
        return MissionStatus.values()[this.status];
    }

    @Override
    public final JsonPatchDocument getPatchDocument() {
        return this.patchDocument;
    }

    @Override
    public final String getModel() {
        return this.metadata.getModelId();
    }

    @Override
    public final List<String> getClasses() {
        return List.of("healthcare");
    }

    @Override
    public final void setFromJsonObject(final JsonObject receivedJsonObj) {
        if (receivedJsonObj.containsKey(DataHub.DATA_HUB_NEW_DATA_DT_ID)) {
            this.setId(receivedJsonObj.getString(DataHub.DATA_HUB_NEW_DATA_DT_ID));
        }
        if (receivedJsonObj.containsKey("place")) {
            this.setPlace(receivedJsonObj.getString("place"));
        }
        if (receivedJsonObj.containsKey("status")) {
            this.setStatus(receivedJsonObj.getInteger("status"));
        }
    }

    public enum MissionStatus {
        /**
         * started = 0.
         */
        STARTED,
        /**
         * movingToPatient = 1.
         */
        MOVING_TO_PATIENT,
        /**
         * rescuing = 2.
         */
        RESCUING,
        /**
         * movingToHospital = 3.
         */
        MOVING_TO_HOSPITAL,
        /**
         * completed = 4.
         */
        COMPLETED;
    }

    private static class MissionMetadata extends DigitalTwinMetadata {
        @JsonProperty("place")
        private DigitalTwinPropertyMetadata place;
        @JsonProperty("status")
        private DigitalTwinPropertyMetadata status;

        public DigitalTwinPropertyMetadata getPlace() {
            return this.place;
        }
        public void setPlace(final DigitalTwinPropertyMetadata place) {
            this.place = place;
        }
        public DigitalTwinPropertyMetadata getStatus() {
            return this.status;
        }
        public void setStatus(final DigitalTwinPropertyMetadata status) {
            this.status = status;
        }
    }
}
