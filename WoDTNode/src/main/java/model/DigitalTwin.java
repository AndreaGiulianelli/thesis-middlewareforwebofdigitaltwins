package model;

import java.util.List;

import com.azure.core.models.JsonPatchDocument;

import io.vertx.core.json.JsonObject;

public interface DigitalTwin {
    String getId();
    void setId(String id);
    JsonPatchDocument getPatchDocument();
    String getModel();
    List<String> getClasses();
    void setFromJsonObject(JsonObject receivedJsonObj);
}
