package model;

import java.util.List;

import com.azure.core.models.JsonPatchDocument;
import com.azure.digitaltwins.core.DigitalTwinPropertyMetadata;
import com.azure.digitaltwins.core.models.DigitalTwinsJsonPropertyNames;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.vertx.core.json.JsonObject;
import verticles.physicaladapter.datahubadapter.DataHub;

public class Patient extends AbstractDigitalTwin {
    @JsonProperty("heartRate")
    private int heartRate;
    @JsonProperty("bloodPressure")
    private double bloodPressure;
    @JsonProperty("bodyTemperature")
    private double bodyTemperature;
    @JsonProperty("patientCode")
    private int patientCode;

    @JsonProperty(DigitalTwinsJsonPropertyNames.DIGITAL_TWIN_METADATA)
    private PatientMetadata metadata = new PatientMetadata();

    /*
     * Json patch document that will be built during the set of the object.
     * In this way we can abstract the name used in the digital twin instance.
     */
    private transient JsonPatchDocument patchDocument = new JsonPatchDocument();

    public Patient() {
        this.metadata.setModelId("dtmi:com:contoso:patient;1");
    }

    /**
     * @return heartRate.
     */
    public int getHeartRate() {
        return this.heartRate;
    }
    /**
     * @param heartRate
     */
    public void setHeartRate(final int heartRate) {
        this.heartRate = heartRate;
        this.patchDocument.appendAdd("/heartRate", heartRate);
    }
    /**
     * @return blood pressure.
     */
    public double getBloodPressure() {
        return this.bloodPressure;
    }
    /**
     * @param bloodPressure
     */
    public void setBloodPressure(final double bloodPressure) {
        this.bloodPressure = bloodPressure;
        this.patchDocument.appendAdd("/bloodPressure", bloodPressure);
    }
    /**
     * @return body temperature.
     */
    public double getBodyTemperature() {
        return this.bodyTemperature;
    }
    /**
     * @param bodyTemperature
     */
    public void setBodyTemperature(final double bodyTemperature) {
        this.bodyTemperature = bodyTemperature;
        this.patchDocument.appendAdd("/bodyTemperature", bodyTemperature);
    }
    /**
     * @return patientCode in integer format.
     */
    public int getPatientCode() {
        return this.patientCode;
    }
    /**
     * @param code
     */
    public void setPatientCode(final int code) {
        this.patientCode = code;
        this.patchDocument.appendAdd("/patientCode", patientCode);
    }
    /**
     * @return metadata
     */
    public PatientMetadata getMetadata() {
        return this.metadata;
    }
    /**
     * @param metadata metadata of twin.
     */
    public void setMetadata(final PatientMetadata metadata) {
        this.metadata = metadata;
    }
    /**
     * @return the enum version of PatientCode
     */
    public PatientCode getPatientCodeEnum() {
        //Get the enum version
        return PatientCode.values()[this.patientCode];
    }
    /**
     * @param obj that contains parameters.
     */
    @Override
    public void setFromJsonObject(final JsonObject obj) {
        if (obj.containsKey(DataHub.DATA_HUB_NEW_DATA_DT_ID)) {
            this.setId(obj.getString(DataHub.DATA_HUB_NEW_DATA_DT_ID));
        }
        if (obj.containsKey("bloodPressure")) {
            this.setBloodPressure(obj.getDouble("bloodPressure"));
        }
        if (obj.containsKey("bodyTemperature")) {
            this.setBodyTemperature(obj.getDouble("bodyTemperature"));
        }
        if (obj.containsKey("heartRate")) {
            this.setHeartRate(obj.getInteger("heartRate"));
        }
        if (obj.containsKey("patientCode")) {
            this.setPatientCode(obj.getInteger("patientCode"));
        }
    }
    /**
     * @return the path document for the Patient.
     */
    @Override
    public JsonPatchDocument getPatchDocument() {
        return this.patchDocument;
    }

    @Override
    public final String getModel() {
        return this.metadata.getModelId();
    }

    /**
     * It's internal to Patient meaning.
     */
    public enum PatientCode {
        /**
         * red = 0.
         */
        RED,
        /**
         * yellow = 1.
         */
        YELLOW,
        /**
         * green = 2.
         */
        GREEN,
        /**
         * white = 3.
         */
        WHITE
    }

    /**
     * It's necessary only for the Digital Twin serialization.
     */
    private static class PatientMetadata extends DigitalTwinMetadata {
        @JsonProperty("heartRate")
        private DigitalTwinPropertyMetadata heartRate;
        @JsonProperty("bloodPressure")
        private DigitalTwinPropertyMetadata bloodPressure;
        @JsonProperty("bodyTemperature")
        private DigitalTwinPropertyMetadata bodyTemperature;
        @JsonProperty("patientCode")
        private DigitalTwinPropertyMetadata patientCode;

        /**
         * @return heart rate metadata.
         */
        public DigitalTwinPropertyMetadata getHeartRate() {
            return this.heartRate;
        }
        /**
         * @param heartRate metadata.
         */
        public void setHeartRate(final DigitalTwinPropertyMetadata heartRate) {
            this.heartRate = heartRate;
        }
        /**
         * @return blood pressure metadata.
         */
        public DigitalTwinPropertyMetadata getBloodPressure() {
            return this.bloodPressure;
        }
        /**
         * @param bloodPressure metadata.
         */
        public void setBloodPressure(final DigitalTwinPropertyMetadata bloodPressure) {
            this.bloodPressure = bloodPressure;
        }
        /**
         * @return body temperature metadata.
         */
        public DigitalTwinPropertyMetadata getBodyTemperature() {
            return this.bodyTemperature;
        }
        /**
         * @param bodyTemperature metadata.
         */
        public void setBodyTemperature(final DigitalTwinPropertyMetadata bodyTemperature) {
            this.bodyTemperature = bodyTemperature;
        }
        /**
         * @return patient code (integer) metadata.
         */
        public DigitalTwinPropertyMetadata getPatientCode() {
            return this.patientCode;
        }
        /**
         * @param patientCode metadata.
         */
        public void setPatientCode(final DigitalTwinPropertyMetadata patientCode) {
            this.patientCode = patientCode;
        }
    }

    @Override
    public final List<String> getClasses() {
        return List.of("healthcare");
    }
}
