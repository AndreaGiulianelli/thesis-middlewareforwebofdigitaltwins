package model.utility;

import model.Ambulance;
import model.DigitalTwin;
import model.Mission;
import model.Patient;

/**
 * A utility class for obtaining class from Digital Twin type.
 */
public final class ModelConverter {

    private ModelConverter() { }

    public static DigitalTwin fromType(final String type) {
        DigitalTwin res;
        switch (type) {
            case "patient":
                res = new Patient();
            break;
            case "ambulance":
                res = new Ambulance();
            break;
            case "mission":
                res = new Mission();
            break;
            default:
                throw new IllegalArgumentException();
        }

        return res;
    }
}
