package application;

import io.vertx.core.Vertx;
import verticles.CatalogComponent;
import verticles.LowerInterface;
import verticles.QualityFidelityEngine;

/**
 * Start point for the Service.
 *
 */
public final class RunCore {

    private RunCore() { }
    /**
     * @param strings params
     */
    public static void main(final String...strings) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new LowerInterface());
        vertx.deployVerticle(new CatalogComponent());
        vertx.deployVerticle(new QualityFidelityEngine());
    }
}
