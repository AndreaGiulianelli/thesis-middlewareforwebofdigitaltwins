package model;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Interface that expose a Digital Twin saved in the Core.
 */
public interface CoreDigitalTwin {
    String getUri();
    String getId();
    URI getBaseUri();
    List<String> getClasses();
    boolean isValid();
    void setValid(boolean valid);
    int getFidelity();
    LocalDateTime getLastUpdate();
    void setLastUpdate(LocalDateTime lastUpdate);
}
