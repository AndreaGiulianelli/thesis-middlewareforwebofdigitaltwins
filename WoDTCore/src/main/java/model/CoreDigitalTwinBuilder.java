package model;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CoreDigitalTwinBuilder {
    private Optional<String> id;
    private Optional<URI> baseUri;
    private Optional<List<String>> classes;
    private Optional<LocalDateTime> lastUpdate;
    private Optional<Integer> fidelityInterval;
    private Optional<Boolean> valid;

    private boolean used;

    public CoreDigitalTwinBuilder() {
        //Set default values for optional fields
        this.classes = Optional.of(new ArrayList<>());
        this.lastUpdate = Optional.of(LocalDateTime.MIN);
        this.fidelityInterval = Optional.of(-1);
        this.valid = Optional.of(true);
    }

    /**
     * @param id - set
     * @return the builder
     */
    public CoreDigitalTwinBuilder setId(final String id) {
        this.id = Optional.ofNullable(id);
        return this;
    }
    /**
     * @param baseUri - set
     * @return the builder
     */
    public CoreDigitalTwinBuilder setBaseUri(final URI baseUri) {
        this.baseUri = Optional.ofNullable(baseUri);
        return this;
    }
    /**
     * @param classes - set
     * @return the builder
     */
    public CoreDigitalTwinBuilder setClasses(final List<String> classes) {
        this.classes = Optional.ofNullable(classes);
        return this;
    }
    /**
     * @param lastUpdate - set
     * @return the builder
     */
    public CoreDigitalTwinBuilder setLastUpdate(final LocalDateTime lastUpdate) {
        this.lastUpdate = Optional.ofNullable(lastUpdate);
        return this;
    }
    /**
     * @param fidelityInterval - set
     * @return the builder
     */
    public CoreDigitalTwinBuilder setFidelityInterval(final int fidelityInterval) {
        this.fidelityInterval = Optional.ofNullable(fidelityInterval);
        return this;
    }
    /**
     * @param valid - set
     * @return the builder
     */
    public CoreDigitalTwinBuilder setValid(final boolean valid) {
        this.valid = Optional.ofNullable(valid);
        return this;
    }

    /**
     * @return a CoreDigitalTwin created from data ingested.
     */
    public CoreDigitalTwin build() {
        if (!this.used) {
            if (this.id.isEmpty() || this.baseUri.isEmpty() || this.classes.isEmpty()
                    || this.lastUpdate.isEmpty() || this.fidelityInterval.isEmpty() || this.valid.isEmpty()) {
                throw new IllegalStateException("Can't create a CoreDigitalTwin, data insufficient");
            } else {
                this.used = true;
                return new CoreDigitalTwinImpl(this.id.get(), this.baseUri.get(), this.classes.get(), 
                        this.lastUpdate.get(), this.fidelityInterval.get(), this.valid.get());
            }
        } else {
            throw new IllegalStateException("Builder already used, create a new one");
        }
    }
}
