package model;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;

public class CoreDigitalTwinImpl implements CoreDigitalTwin {

    private final String id;
    private final URI baseUri;
    private final List<String> classes;
    private boolean valid;
    private LocalDateTime lastUpdate;
    private int fidelityInterval;

    protected CoreDigitalTwinImpl(final String id, final URI baseUri, final List<String> classes,
            final LocalDateTime lastUpdate, final int fidelityInterval, final boolean valid) {
        this.id = id;
        this.baseUri = baseUri;
        this.classes = List.copyOf(classes);
        this.valid = valid;
        this.lastUpdate = lastUpdate;
        this.fidelityInterval = fidelityInterval;
    }

    @Override
    public final String getUri() {
        return this.baseUri + this.id;
    }

    @Override
    public final String getId() {
        return this.id;
    }

    @Override
    public final URI getBaseUri() {
        return this.baseUri;
    }

    @Override
    public final List<String> getClasses() {
        //return a safe copy
        return List.copyOf(this.classes);
    }

    @Override
    public final boolean isValid() {
        return this.valid;
    }

    public final void setValid(final boolean valid) {
        this.valid = valid;
    }

    @Override
    public final int getFidelity() {
        return this.fidelityInterval;
    }

    @Override
    public final LocalDateTime getLastUpdate() {
        return this.lastUpdate;
    }

    @Override
    public final void setLastUpdate(final LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}
