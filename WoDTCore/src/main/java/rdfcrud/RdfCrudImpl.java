package rdfcrud;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.tdb.TDBFactory;

import io.vertx.core.json.JsonObject;
import model.CoreDigitalTwin;
import model.CoreDigitalTwinBuilder;

public class RdfCrudImpl implements RdfCrud {

    private static final String RDF_STORE_PATH = "RdfStore";
    private static final String NAMED_GRAPH_CATALOG = "catalog";
    private static final String PROPERTY_PREFIX = "http://example.com/";
    private Dataset dataset;

    public RdfCrudImpl() {
        this.dataset = TDBFactory.createDataset(RdfCrudImpl.RDF_STORE_PATH);
    }

    @Override
    public final boolean save(final CoreDigitalTwin digitalTwin) {
        this.dataset.begin(ReadWrite.WRITE);
        try {
            Model model = this.dataset.getDefaultModel();
            //Create the resource for the new Digital Twin
            Resource newDigitalTwinRes = model.createResource(digitalTwin.getUri());
            //Create the properties
            Property idProp = model.createProperty(RdfCrudImpl.PROPERTY_PREFIX + "id");
            Property baseProp = model.createProperty(RdfCrudImpl.PROPERTY_PREFIX + "base");
            Property classProp = model.createProperty(RdfCrudImpl.PROPERTY_PREFIX + "class");
            Property validProp = model.createProperty(RdfCrudImpl.PROPERTY_PREFIX + "valid");
            Property fidelityProp = model.createProperty(RdfCrudImpl.PROPERTY_PREFIX + "fidelity");
            Property lastUpdateProp = model.createProperty(RdfCrudImpl.PROPERTY_PREFIX + "lastUpdate");

            //Save the values
            newDigitalTwinRes.addProperty(idProp, digitalTwin.getId())
            .addProperty(baseProp, digitalTwin.getBaseUri().toString())
            .addLiteral(validProp, digitalTwin.isValid())
            .addLiteral(fidelityProp, digitalTwin.getFidelity())
            .addLiteral(lastUpdateProp, digitalTwin.getLastUpdate().toString());

            //Iterate classes of Digital Twins to save them.
            digitalTwin.getClasses().forEach((singleClass) -> {
                newDigitalTwinRes.addProperty(classProp, singleClass);
            });

            //Commit the transaction
            this.dataset.commit();
        } catch (Exception e) {
            this.dataset.abort();
            return false;
        } finally {
            //Close the transaction
            this.dataset.end();
        }
        return true;
    }

    @Override
    public final boolean delete(final String uri) {
        this.dataset.begin(ReadWrite.WRITE);
        try {
            Model model = this.dataset.getDefaultModel();
            Resource res = model.getResource(uri);
            System.out.println(res.getURI());
            //Remove all statements where res is subject
            model.removeAll(res, null, null);
            //Remove all statements where res is object (relationships)
            model.removeAll(null, null, res);

            //Commit the transaction
            this.dataset.commit();
        } catch (Exception e) {
            this.dataset.abort();
            System.out.println(e);
            return false;
        } finally {
            //Close the transaction
            this.dataset.end();
        }
        return true;
    }

    @Override
    public final CoreDigitalTwin getDigitalTwin(final String uri) {
        final CoreDigitalTwinBuilder builder = new CoreDigitalTwinBuilder();
        this.dataset.begin(ReadWrite.READ);
        try {
            Model model = this.dataset.getDefaultModel();
            Resource res = model.getResource(uri);

            //Reference property
            Property idProp = model.createProperty(RdfCrudImpl.PROPERTY_PREFIX + "id");
            Property baseProp = model.createProperty(RdfCrudImpl.PROPERTY_PREFIX + "base");
            Property classProp = model.createProperty(RdfCrudImpl.PROPERTY_PREFIX + "class");
            Property validProp = model.createProperty(RdfCrudImpl.PROPERTY_PREFIX + "valid");
            Property fidelityProp = model.createProperty(RdfCrudImpl.PROPERTY_PREFIX + "fidelity");
            Property lastUpdateProp = model.createProperty(RdfCrudImpl.PROPERTY_PREFIX + "lastUpdate");

            List<String> classes = new ArrayList<>();
            res.getProperty(classProp).getList().asJavaList().forEach((x) -> classes.add(x.toString()));

            builder.setId(res.getProperty(idProp).getString())
            .setBaseUri(URI.create(res.getProperty(baseProp).getString()))
            .setClasses(classes)
            .setValid(res.getProperty(validProp).getBoolean())
            .setFidelityInterval(res.getProperty(fidelityProp).getInt())
            .setLastUpdate(LocalDateTime.parse(res.getProperty(lastUpdateProp).getString()));

        } catch (Exception e) {
            this.dataset.abort();
            System.out.println(e);
            return null;
        } finally {
            //Close the transaction
            this.dataset.end();
        }
        return builder.build();
    }

    @Override
    public final JsonObject query(final String query) {
        this.dataset.begin(ReadWrite.READ);
        JsonObject jsonResult = new JsonObject();
        try {
            try (QueryExecution qExec = QueryExecutionFactory.create(query, this.dataset)) {
                ResultSet rs = qExec.execSelect();
                OutputStream outputStream = new ByteArrayOutputStream();
                //Output in JSON: https://www.w3.org/TR/rdf-sparql-json-res/
                ResultSetFormatter.outputAsJSON(outputStream, rs);
                jsonResult = new JsonObject(outputStream.toString());
            } 
        } catch (Exception e) {
            this.dataset.abort();
            System.out.println(e);
          jsonResult.put("error", "invalid query");
        } finally {
            this.dataset.end();
        }
        return jsonResult;
    }

    @Override
    public final boolean updateValidity(final String uri, final boolean valid) {
        this.dataset.begin(ReadWrite.WRITE);
        try {
            Model model = this.dataset.getDefaultModel();
            Resource res = model.getResource(uri);
            Property validProp = model.createProperty(RdfCrudImpl.PROPERTY_PREFIX + "valid");
            Statement stmt = res.getProperty(validProp);
            stmt.changeLiteralObject(valid);

            //Commit the transaction
            this.dataset.commit();
        } catch (Exception e) {
            this.dataset.abort();
            System.out.println(e);
            return false;
        } finally {
            //Close the transaction
            this.dataset.end();
        }
        return true;
    }

    @Override
    public final boolean updateLastUpdate(final String uri, final LocalDateTime lastUpdate) {
        this.dataset.begin(ReadWrite.WRITE);
        try {
            Model model = this.dataset.getDefaultModel();
            Resource res = model.getResource(uri);
            Property lastUpdateProp = model.createProperty(RdfCrudImpl.PROPERTY_PREFIX + "lastUpdate");
            Statement stmt = res.getProperty(lastUpdateProp);
            stmt.changeObject(lastUpdate.toString());

            //Commit the transaction
            this.dataset.commit();
        } catch (Exception e) {
            this.dataset.abort();
            System.out.println(e);
            return false;
        } finally {
            //Close the transaction
            this.dataset.end();
        }
        return true;
    }

    /**
     * Get count of statements.
     */
    public void explore() {
        this.dataset.begin(ReadWrite.READ);
        try {
            try (QueryExecution qExec = QueryExecutionFactory.create(
                    "SELECT (count(*) AS ?count) { ?s ?p ?o} LIMIT 10", 
                  this.dataset)) {
                ResultSet rs = qExec.execSelect();
                ResultSetFormatter.out(rs);
            } 
        } finally {
            this.dataset.end();
        }
    }

    /**
     * List statements.
     */
    public void listStatement() {
        this.dataset.begin(ReadWrite.READ);
        try {
            Model model = this.dataset.getDefaultModel();
            //list the statements in the Model
            StmtIterator iter = model.listStatements();

            // print out the predicate, subject and object of each statement
            while (iter.hasNext()) {
                Statement stmt      = iter.nextStatement();  // get next statement
                Resource  subject   = stmt.getSubject();     // get the subject
                Property  predicate = stmt.getPredicate();   // get the predicate
                RDFNode   object    = stmt.getObject();      // get the object

                System.out.print(subject.toString());
                System.out.print(" " + predicate.toString() + " ");
                if (object instanceof Resource) {
                   System.out.print(object.toString());
                } else {
                    // object is a literal
                    System.out.print(" \"" + object.toString() + "\"");
                }

                System.out.println(" .");
            }
        } finally {
            this.dataset.end();
        }
    }

}
