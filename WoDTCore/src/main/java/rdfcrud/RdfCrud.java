package rdfcrud;

import java.time.LocalDateTime;

import io.vertx.core.json.JsonObject;
import model.CoreDigitalTwin;

public interface RdfCrud {
    boolean save(CoreDigitalTwin digitalTwin);
    boolean delete(String uri);
    CoreDigitalTwin getDigitalTwin(String uri);
    JsonObject query(String query);
    boolean updateValidity(String uri, boolean valid);
    boolean updateLastUpdate(String uri, LocalDateTime lastUpdate);
}
