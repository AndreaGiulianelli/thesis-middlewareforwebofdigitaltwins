package verticles;

import java.time.LocalDateTime;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import rdfcrud.RdfCrud;
import rdfcrud.RdfCrudImpl;

public class QualityFidelityEngine extends AbstractVerticle {

    private static final int UPDATE_TICK = 1000;
    private static final String QUERY = "prefix ex: <http://example.com/> \n"
            + "SELECT ?id ?fidelity ?lastUpdate ?valid ?base "
            + "WHERE { "
            + "?id ex:fidelity ?fidelity . "
            + "?id ex:lastUpdate ?lastUpdate . "
            + "?id ex:valid ?valid . "
            + "?id ex:base ?base ."
            + " }";

    private RdfCrud rdfCrud;

    @Override
    public final void start() {
        try {
            this.rdfCrud = new RdfCrudImpl();
        } catch (Exception e) {
            System.out.println(e);
            return;
        }
        System.out.println("RDFStore initialized");
        final EventBus eventBus = vertx.eventBus();

        //Set periodic actions
        /*
         * Use setTimer instead of setPeriodic in order to avoid stack up timers 
         * when can't finish the check in time
         */
        vertx.setPeriodic(QualityFidelityEngine.UPDATE_TICK, x -> {
            final JsonObject jsonResult = this.rdfCrud.query(QualityFidelityEngine.QUERY);
            //System.out.println(jsonResult.encode());
            jsonResult.getJsonObject("results").getJsonArray("bindings").iterator().forEachRemaining(obj -> {
               //Get data from json event.
               final JsonObject singleObj = new JsonObject(obj.toString());
               String uri = singleObj.getJsonObject("id").getString("value");
               int fidelity = Integer.parseInt(singleObj.getJsonObject("fidelity").getString("value"));
               boolean wasValid = Boolean.parseBoolean(singleObj.getJsonObject("valid").getString("value"));
               String baseUri = singleObj.getJsonObject("base").getString("value");
               LocalDateTime lastUpdate = LocalDateTime.parse(singleObj.getJsonObject("lastUpdate").getString("value"));

               //If fidelity is < 0 then the Digital Twin is always valid
               if (fidelity > 0) {
                   //(LocalDateTime - fidelity) < lastUpdate ? VALID : NOT VALID 
                   LocalDateTime lowerBoundDateTime = LocalDateTime.now().minusSeconds(fidelity / 1000); //fidelity is in milliseconds
                   //Check that the Digital Twin is in a valid state
                   //Also check if the Digital Twin need a change in state or it's already in the desired state.
                   if (lowerBoundDateTime.isBefore(lastUpdate) && !wasValid) {
                       //OK VALID, UPDATE VALID to true
                       this.rdfCrud.updateValidity(uri, true);
                       //Debug
                       System.out.println(uri + "\n\t Now: " + LocalDateTime.now().toString() + "\n\tLast: " + lastUpdate.toString() + "\n\tFidelity: " + fidelity + "\n\tWasValid: " + wasValid + "\n\tNow: VALID");

                       //Create message to inform the organization node the change of validity of its Digital Twin.
                       final JsonObject msgToNode = new JsonObject();
                       msgToNode.put(LowerInterface.TYPE_LABEL, EventType.VALIDITY)
                                .put(LowerInterface.ID_LABEL, uri.replaceAll(baseUri, ""))
                                .put(LowerInterface.VALID_LABEL, true);
                       eventBus.send(LowerInterface.SIGNAL_TO_NODE, new JsonObject().put(LowerInterface.ORG_LABEL, baseUri).put(LowerInterface.MSG_LABEL, msgToNode.encode()));
                   } else if (lowerBoundDateTime.isAfter(lastUpdate) && wasValid) {
                       //INVALID, UPDATE VALID to false
                       this.rdfCrud.updateValidity(uri, false);
                       //Debug
                       System.out.println(uri + "\n\t Now: " + LocalDateTime.now().toString() + "\n\tLast: " + lastUpdate.toString() + "\n\tFidelity: " + fidelity + "\n\tWasValid: " + wasValid + "\n\tNow: NOT VALID");

                       //Create message to inform the organization node the change of validity of its Digital Twin.
                       final JsonObject msgToNode = new JsonObject();
                       msgToNode.put(LowerInterface.TYPE_LABEL, EventType.VALIDITY)
                               .put(LowerInterface.ID_LABEL, uri.replaceAll(baseUri, ""))
                               .put(LowerInterface.VALID_LABEL, false);
                       eventBus.send(LowerInterface.SIGNAL_TO_NODE, new JsonObject().put(LowerInterface.ORG_LABEL, baseUri).put(LowerInterface.MSG_LABEL, msgToNode.encode()));
                   }
               }
            });
        });
    }
}
