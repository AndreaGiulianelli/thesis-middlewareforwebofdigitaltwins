package verticles;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.NetServer;
import io.vertx.core.net.NetSocket;

public class LowerInterface extends AbstractVerticle {

    private static final int TCP_SOCKET_PORT = 1234;
    /**
     * Label for type in event body sent to Core Event Dispatcher.
     */
    public static final String TYPE_LABEL = "type";
    /**
     * Label for id in event body sent to Core Event Dispatcher.
     */
    public static final String ID_LABEL = "id";
    /**
     * Label for fidelity in event body sent to Core Event Dispatcher.
     */
    public static final String FIDELITY_LABEL = "fidelity";
    /**
     * Label for validity in event body sent to Core Event Dispatcher.
     */
    public static final String VALID_LABEL = "valid";
    /**
     * Label to specify the org id in event body.
     */
    public static final String ORG_LABEL = "org";
    /**
     * Label to specify the classes of the Digital Twin in event body.
     */
    public static final String CLASSES_LABEL = "classes";
    /**
     * Event bus address to send a message to a node.
     */
    public static final String SIGNAL_TO_NODE = "lowerinterface.tonodesocket";
    /**
     * Label to specify the msg to send.
     */
    public static final String MSG_LABEL = "msg";

    private EventBus eventBus;

    /*
     * Map that save the mapping from an uri to the corresponding socket.
     * In order to save mapping, each time there is a message on the socket, save the corresponding org in the map
     * if it isn't already present.
     * 
     * It's the easy method, full of bugs, but it's for explanatory purposes.
     * In real implementation there is the need for an handshake mechanism.
     */
    private Map<URI, NetSocket> mapping = new HashMap<>();

    @Override
    public final void start() {
        this.eventBus = vertx.eventBus();
        NetServer server = vertx.createNetServer();
        System.out.println("Creating server socket...");
        server.connectHandler(socket -> {
            System.out.println("New connection from WoDT NODE");
            socket.handler(buffer -> {
                this.handleNewEvent(buffer.toJsonObject(), socket);
            });
        }).listen(LowerInterface.TCP_SOCKET_PORT, "localhost", res -> {
            if (res.succeeded()) {
                System.out.println("WoDT Core Lower Interface socket created");
            } else {
                System.out.println("Error: " + res.cause().getMessage());
                System.exit(0);
            }
        });

        this.eventBus.consumer(LowerInterface.SIGNAL_TO_NODE, msg -> {
           final JsonObject jsonObj = new JsonObject(msg.body().toString());
           final String uri = jsonObj.getString(LowerInterface.ORG_LABEL);
           final String msgToSend = jsonObj.getString(LowerInterface.MSG_LABEL);
           if (this.mapping.containsKey(URI.create(uri))) {
               this.mapping.get(URI.create(uri)).write(msgToSend);
           }
        });
    }

    private void handleNewEvent(final JsonObject jsonEvent, final NetSocket socket) {
        System.out.println("WoDT CORE Lower Interface - received msg - " + jsonEvent.encode());
        final EventType eventType = EventType.valueOf(jsonEvent.getString(LowerInterface.TYPE_LABEL));
        switch (eventType) {
            case CREATE:
                //Check data
                if (jsonEvent.containsKey(LowerInterface.ORG_LABEL) && jsonEvent.containsKey(LowerInterface.TYPE_LABEL)
                        && jsonEvent.containsKey(LowerInterface.ID_LABEL) && jsonEvent.containsKey(LowerInterface.FIDELITY_LABEL)
                        && jsonEvent.containsKey(LowerInterface.CLASSES_LABEL)) {
                    //Inform catalog
                    this.eventBus.send(CatalogComponent.CREATE_DIGITAL_TWIN, jsonEvent.encode());
                } // else discard event
                break;
            case DELETE:
                //Check data
                if (jsonEvent.containsKey(LowerInterface.ORG_LABEL) && jsonEvent.containsKey(LowerInterface.TYPE_LABEL)
                        && jsonEvent.containsKey(LowerInterface.ID_LABEL)) {
                    //Inform catalog
                    this.eventBus.send(CatalogComponent.DELETE_DIGITAL_TWIN, jsonEvent.encode());
                } // else discard event
                break;
            case UPDATE:
                //Inform quality and fidelity handler and catalog (for the metadata sub-component).
                //Check data
                if (jsonEvent.containsKey(LowerInterface.ORG_LABEL) && jsonEvent.containsKey(LowerInterface.TYPE_LABEL)
                        && jsonEvent.containsKey(LowerInterface.ID_LABEL)) {
                    //Inform catalog
                    this.eventBus.send(CatalogComponent.UPDATED_DIGITAL_TWIN, jsonEvent.encode());
                } // else discard event
                break;
            default:
                return;
        }

        //Save the mapping URI - SOCKET
        final URI uri = URI.create(jsonEvent.getString(LowerInterface.ORG_LABEL));
        if (!this.mapping.containsKey(uri)) {
            this.mapping.put(uri, socket);
            System.out.println("Save mapping for uri: " + uri.toString());
        }
    }
    
    
}
