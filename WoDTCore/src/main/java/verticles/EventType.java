package verticles;

/**
 * Type of event internal to the organization to signal the CORE.
 */
public enum EventType {
    /**
     * Digital Twin created.
     */
    CREATE,
    /**
     * Digital Twin deleted.
     */
    DELETE,
    /**
     * Digital Twin updated.
     */
    UPDATE,
    /**
     * Digital Twin validity.
     */
    VALIDITY;
}
