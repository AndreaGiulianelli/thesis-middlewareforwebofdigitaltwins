package verticles;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import model.CoreDigitalTwin;
import model.CoreDigitalTwinBuilder;
import rdfcrud.RdfCrud;
import rdfcrud.RdfCrudImpl;

public class CatalogComponent extends AbstractVerticle {

    /**
     * Event bus address that Lower Interface use to communicate the creation of a Digital Twin in an organization.
     */
    public static final String CREATE_DIGITAL_TWIN = "catalog.createdigitaltwin";
    /**
     * Event bus address that Lower Interface use to communicate the deletion of a Digital Twin in an organization.
     */
    public static final String DELETE_DIGITAL_TWIN = "catalog.deletedigitaltwin";
    /**
     * Event bus address that Lower Interface use to communicate the update of a Digital Twin in an organization.
     */
    public static final String UPDATED_DIGITAL_TWIN = "catalog.updateddigitaltwin";
    private EventBus eventBus;
    private RdfCrud rdfCrud;

    @Override
    public final void start() { 
        this.eventBus = vertx.eventBus();
        this.rdfCrud = new RdfCrudImpl();

        //New Digital Twin event consumer
        this.eventBus.consumer(CatalogComponent.CREATE_DIGITAL_TWIN, message -> {
            System.out.println("CATALOG COMPONENT - Processing" + message.body().toString());
            final JsonObject jsonObj = new JsonObject(message.body().toString());
            final List<String> digitalTwinClasses = new ArrayList<>();
            jsonObj.getJsonArray(LowerInterface.CLASSES_LABEL).spliterator().forEachRemaining((cls) -> {
                digitalTwinClasses.add(cls.toString());
            });
            final CoreDigitalTwinBuilder builder = new CoreDigitalTwinBuilder();
            builder.setId(jsonObj.getString(LowerInterface.ID_LABEL))
                    .setBaseUri(URI.create(jsonObj.getString(LowerInterface.ORG_LABEL)))
                    .setClasses(digitalTwinClasses)
                    .setLastUpdate(LocalDateTime.now())
                    .setFidelityInterval(jsonObj.getInteger(LowerInterface.FIDELITY_LABEL));

            final CoreDigitalTwin digitalTwinToCreate = builder.build(); 
            ((RdfCrudImpl) this.rdfCrud).explore();
            this.rdfCrud.save(digitalTwinToCreate);
            ((RdfCrudImpl) this.rdfCrud).explore();
        });

        //Delete Digital Twin event consumer
        this.eventBus.consumer(CatalogComponent.DELETE_DIGITAL_TWIN, message -> {
            System.out.println("CATALOG COMPONENT - Processing" + message.body().toString());
            final JsonObject jsonObj = new JsonObject(message.body().toString());

            final CoreDigitalTwinBuilder builder = new CoreDigitalTwinBuilder();
            builder.setId(jsonObj.getString(LowerInterface.ID_LABEL))
                    .setBaseUri(URI.create(jsonObj.getString(LowerInterface.ORG_LABEL)));
            final CoreDigitalTwin digitalTwinToRemove = builder.build();
            ((RdfCrudImpl) this.rdfCrud).explore();
            this.rdfCrud.delete(digitalTwinToRemove.getUri());
            ((RdfCrudImpl) this.rdfCrud).explore();
        });

        //Updated Digital Twin event consumer
        this.eventBus.consumer(CatalogComponent.UPDATED_DIGITAL_TWIN, message -> {
            System.out.println("CATALOG COMPONENT - Processing" + message.body().toString());
            final JsonObject jsonObj = new JsonObject(message.body().toString());
            final CoreDigitalTwinBuilder builder = new CoreDigitalTwinBuilder();
            builder.setId(jsonObj.getString(LowerInterface.ID_LABEL))
                    .setBaseUri(URI.create(jsonObj.getString(LowerInterface.ORG_LABEL)));
            final CoreDigitalTwin digitalTwinToRemove = builder.build();
            this.rdfCrud.updateLastUpdate(digitalTwinToRemove.getUri(), LocalDateTime.now());
            ((RdfCrudImpl) this.rdfCrud).listStatement();
        });
    }
}
